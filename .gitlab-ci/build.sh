
echo "date stamp version this change"
VERSION_NO=v$(cat ./version)

date > ./views/pages/misc/version.html
echo " | version: $VERSION_NO" >> ./views/pages/misc/version.html

echo "Check User ..."
aws sts get-caller-identity

echo "Docker login  ..."
aws ecr get-login-password --region $AWS_DEFAULT_REGION  --endpoint $END_POINT | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID$DOCKER_REGISTRY

echo "Building Image ..."
# docker build -t $IMAGE_NAME:$IMAGE_TAG .
docker build -t $IMAGE_NAME:$VERSION_NO .

echo "Tagging Image ..."
docker tag $IMAGE_NAME:$VERSION_NO $AWS_ACCOUNT_ID$DOCKER_REGISTRY/$IMAGE_NAME:$IMAGE_TAG

echo "Tagging Image ..."
docker tag $IMAGE_NAME:$VERSION_NO $AWS_ACCOUNT_ID$DOCKER_REGISTRY/$IMAGE_NAME:$VERSION_NO