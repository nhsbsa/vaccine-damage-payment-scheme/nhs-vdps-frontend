
echo "Validate Release"
VERSION_NO=v$(cat ./version)

echo $CI_COMMIT_REF_NAME
echo $PROD_BRANCH

if [ $(git tag -l "$VERSION_NO") ]; then
   echo "Version $VERSION_NO already exists, This is a production deployment please update the version"
   exit 1
else
   echo "Version $VERSION_NO is valid, This is a production deployment"
fi