#NHS Vaccine Damage Payment Schedule (VDPS) Modernisation

##About the NHS VDPS
The Vaccine Damage Payment Scheme (VDPS) is a digital service for claiming payment for someone severely disabled by the effects of a vaccination against certain disease. 

This is a web application written using the NodeJS framework for the online form that communicates to AWS Lambdas to perform data processing and storage into a temporary Dynamo data store before submitting to Dynamics system for case workers to handle individual cases.  

##Quick start
Before building the application you will need to have Docker and Docker-compose installed. If you do not already have these installed, you can download the latest versions from the official Docker website.

After you have Docker and Docker-compose installed, navigate to the root directory and run 'npm install' to install the requires packages.

###Steps Building the application
Commands within the package.json are available to run and build the application.

###Running the application
+ To start the application in development enviroment run 'npm run dev' command.

##Contributions
We operate a [code of conduct](CODE_OF_CONDUCT.md) for all contributors.

See our [contributing guide](CONTRIBUTING.md) for guidance on how to contribute.

##License
Released under the [Apache 2 license](LICENCE.txt).

