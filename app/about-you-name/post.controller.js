'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const dataRepository = require('../../common/schemas/Data')


module.exports = () => {
  return {
    render: async (req, res) => {
            req.session.section = 'YourDetails'
  try {
              const repo = (await cdRepository()).repository
              const cd = await repo.fetch(req.cookies.sessionID)
              cd.CdTitle = req.body.CdTitle
              cd.CdSurname = req.body.CdSurname
              cd.CdMiddlename = req.body.CdMiddlename ? req.body.CdMiddlename : null
              cd.CdForename = req.body.CdForename
              await repo.save(cd)
               if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                  res.redirect('about-you-name')
                  return
                }

                const dataRepo = (await dataRepository()).repository
               const data = (await dataRepo.fetch(req.cookies.sessionID)).toJSON()
                if(data.ApplyingFor === 'Myself'){
                    res.redirect('about-you-dob')
                } else if(data.ApplyingFor === 'Deceased'){
                    res.redirect('about-you-died-relationship')
                }else{
                    res.redirect('about-you-under16-relationship')
                }
                return
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.message
              })
            }
          res.redirect('about-you-name')
          return

    }
  }
}
