const { check, validationResult } = require('express-validator')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const aboutYouNameRules = () => {
  return [
    check('CdForename')
        .trim()
        .isLength({ min: 2 })
        .withMessage('common.errors.E0024')
        .bail()
        .isLength({ max: 100 })
        .withMessage('common.errors.E0025')
        .bail()
        .matches(/^[A-Za-z \'\-]+$/)
        .withMessage('common.errors.E0026'),
    check('CdMiddlename')
        .trim()
        .optional({ checkFalsy: true, nullable: true })
        .isLength({ min: 2 })
        .withMessage('common.errors.E0027')
        .isLength({ max: 100 })
        .withMessage('common.errors.E0028')
        .bail()
        .matches(/^[A-Za-z \'\-]+$/)
        .withMessage('common.errors.E0029'),
    check('CdSurname')
        .trim()
        .isLength({ min: 2 })
        .withMessage('common.errors.E0030')
        .bail()
        .isLength({ max: 100 })
        .withMessage('common.errors.E0031')
        .bail()
        .matches(/^[A-Za-z \'\-]+$/)
        .withMessage('common.errors.E0032')
  ]
}
var xss = require("xss");

// while this function executes the actual rules against the request body
const aboutYouNameValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)
    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
    }else{
      req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    if(!errors.isEmpty()){
        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
        applicationStatus.YourDetails = 'ToDo'
        await applicationStatusRepo.save(applicationStatus)
    }
    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            }
        }
    }
    // move to next middleware
    next()
  }
}
module.exports = {
  aboutYouNameValidation,
  aboutYouNameRules
}
