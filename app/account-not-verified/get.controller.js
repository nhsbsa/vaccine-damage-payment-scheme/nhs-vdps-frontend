'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const metaRepository = require('../../common/schemas/MetaData')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
module.exports = () => {
  return {
    render: async (req, res) => {
      try {
        req.session.errors = [{
          msg: `common.errors.E0117`
        }]

        const templateBody = findFile('index')

        const markdownRender = markdown(templateBody.toString())

        const params = {
          content: markdownRender,
        }
        res.render('app/index/index', params)
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        return res.redirect('back')
      }
    }
  }
}
