const { req, res } = require('express');
const postController = require('./post.controller');
const vaccineRepository = require('../../common/schemas/Vaccinations');
const metaRepository = require('../../common/schemas/MetaData');

jest.mock('../../common/schemas/MetaData');
jest.mock('../../common/schemas/Vaccinations');


describe('Post Controller', () => {
    test('should redirect to add-vaccine-location page', async () => {
        await postController.post(req, res);
        expect(res.redirect).toHaveBeenCalledTimes(1);
        expect(res.redirect.mock.calls[0][0]).toEqual('add-vaccine-location');
    });

    test('should redirect to add-covid19-type page when req is invalid', async () => {
        await postController.post({}, res);
        expect(res.redirect).toHaveBeenCalledTimes(1);
        expect(res.redirect.mock.calls[0][0]).toEqual('add-covid19-type');
    });

    test('should call fetch metaRepo with the correct param', async () => {
        const { repository: metaRepo } = await metaRepository();

        await postController.post(req, res);
        expect(metaRepo.fetch).toHaveBeenCalledTimes(1);
        expect(metaRepo.fetch.mock.calls[0][0]).toEqual('1234');
    });

    test('should call vaccRepo.fetch with the correct param', async () => {
        const { repository: vaccRepo } = await vaccineRepository();

        await postController.post(req, res);
        expect(vaccRepo.fetch).toHaveBeenCalledTimes(1);
        expect(vaccRepo.fetch.mock.calls[0][0]).toEqual('1234:1');
    });

    test('should call vaccRepo.fetch when vaccineCount = 3', async () => {
        const { repository: vaccRepo } = await vaccineRepository();
        req.body.vaccineCount = 3;
        await postController.post(req, res);
        expect(vaccRepo.fetch).toHaveBeenCalledTimes(1);
        expect(vaccRepo.fetch.mock.calls[0][0]).toEqual('1234:3');
    });

    test.each([
        'AstraZeneca',
        'Pfizer',
        'Moderna',
        "Other / don't know"
    ])('should call vaccRepo.save when Covid19Type = %s', async (type) => {
        const { repository: vaccRepo } = await vaccineRepository();
        req.body.type = type;
        await postController.post(req, res);
        expect(vaccRepo.save).toHaveBeenCalledTimes(1);
        expect(vaccRepo.save.mock.calls[0][0]).toEqual({
            Covid19Type: type
        });
    });
});
