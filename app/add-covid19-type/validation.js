const VALID_TYPES = [
  "AstraZeneca",
  "Pfizer",
  "Moderna",
  "Other / don't know"
];

module.exports.addCovid19TypeValidation = () => {
  return (req, res, next) => {
    const errors = [];

    if (!req.body.type || !VALID_TYPES.includes(req.body.type)) {
        errors.push({
          route: req.route.path,
          msg: 'common.errors.E0035',
          param: 'type'
        })
      }



    if(errors.length > 0){
        req.session.errors = errors;
        return res.redirect('add-covid19-type')
    }
    delete req.session.errors

    next()
}
}