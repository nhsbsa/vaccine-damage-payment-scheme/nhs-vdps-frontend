const { addCovid19TypeValidation } = require('./validation');
const { req, res, next } = require('express');

describe('addCovid19TypeValidation', () => {
    test.each([
        "AstraZeneca",
        "Pfizer",
        "Moderna",
        "Other / don't know"
    ])('should pass validation when type is %s', (type) => {
        req.body.type = type;
        addCovid19TypeValidation()(req, res, next);
        expect(next).toHaveBeenCalledTimes(1);
    });

    it('should fail validation for invalid types', () => {
        req.body.type = 'Invalid Type';
        addCovid19TypeValidation()(req, res, next);
        expect(req.session.errors).toEqual([
            {
                "msg": "common.errors.E0035",
                "param": "type",
                "route": "/path",
            },
        ]);
        expect(res.redirect.mock.calls[0][0]).toEqual('add-covid19-type');
    });

    it('should fail validation for empty types', () => {
        req.body.type = '';
        addCovid19TypeValidation()(req, res, next);
        expect(req.session.errors).toEqual([
            {
                "msg": "common.errors.E0035",
                "param": "type",
                "route": "/path",
            },
        ]);
        expect(res.redirect.mock.calls[0][0]).toEqual('add-covid19-type');
    });
});
