const { addVaccineCountryValidation } = require('./validation');
const { req, res, next } = require('express');

describe('addVaccineCountryValidation', () => {
    test.each([
        "England",
        "Northern Ireland",
        "Scotland",
        "Wales",
        "Isle of Man",
    ])('should pass validation when type is %s', (country) => {
        req.body.country = country;
        addVaccineCountryValidation()(req, res, next);
        expect(next).toHaveBeenCalledTimes(1);
    });

    it('should fail validation for invalid country', () => {
        req.body.country = 'Invalid country';
        addVaccineCountryValidation()(req, res, next);
        expect(req.session.errors).toEqual([
            {
                "msg": "common.errors.E0016",
                "param": "country",
                "route": "/path",
            },
        ]);
        expect(res.redirect.mock.calls[0][0]).toEqual('/path');
    });

    it('should fail validation for empty country', () => {
        req.body.country = '';
        addVaccineCountryValidation()(req, res, next);
        expect(req.session.errors).toEqual([
            {
                "msg": "common.errors.E0016",
                "param": "country",
                "route": "/path",
            },
        ]);
        expect(res.redirect.mock.calls[0][0]).toEqual('/path');
    });
});
