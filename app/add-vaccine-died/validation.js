const { check, validationResult } = require('express-validator')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const addVaccineDiedQuestionRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const addVaccineDiedQuestionValidation = () => {
  return async (req, res, next) => {
   // execute the rules
   const errors = validationResult(req)
   if (req.body.vaccineReceived === undefined) {
     errors.errors.push({
       msg: 'common.errors.E0035',
       param: 'Coronavirus%20(COVID-19)',
       section: 'vaccineReceived'
     })
   }
    if(!errors.isEmpty()){
        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
        applicationStatus.VaccinesReceived = 'ToDo'

        await applicationStatusRepo.save(applicationStatus)
    }
    if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
        req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
    }else{
        req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    if(errors.isEmpty()){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
    }
   // check if any of the request body failed the requirements set in validation rules
   if (!errors.isEmpty()) {
     // heres where you send the errors or do whatever you please with the error, in  your case
     res.redirect('add-vaccine-died')
     return
   }
   // if everything went well, and all rules were passed, then move on to the next middleware
    if(req.session.errors){
        if(!req.session.errors.length){
            req.session.errors = null
        }
    }
   next()
  }
}
module.exports = {
  addVaccineDiedQuestionRules,
  addVaccineDiedQuestionValidation
}
