'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const vaccineRepository = require('../../common/schemas/Vaccinations')
const metaRepository = require('../../common/schemas/MetaData')
const querystring = require('node:querystring');
module.exports = () => {
  return {
    render: async (req, res) => {

      req.session.section = 'VaccinesReceived'

      try {
        const metaRepo = (await metaRepository()).repository
        const metaData = await metaRepo.fetch(req.cookies.sessionID)
        let count = metaData.NumberOfVaccines;

        if(!count){
            count = 1
        } else {
            count = count +1
        }
        const repo = (await vaccineRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID + ':' + count)
        data.VaccinationType = req.body.vaccineReceived
        data.VaccinationCount = Number(count)
        data.SessionID = req.cookies.sessionID
        data.DisplayVaccination = false
        await repo.save(data)

        metaData.NumberOfVaccines = Number(count)
        await metaRepo.save(metaData)
    
        if(req.body.vaccineReceived === "Coronavirus (COVID-19)"){
          return res.redirect('add-covid19-type')
        }
        
        return res.redirect('add-vaccine-location')
        
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        return res.redirect('add-vaccine-other')
      }
    }
  }
}
