const { check, validationResult } = require('express-validator')
const metaRepository = require('../../common/schemas/MetaData')

// this sets the rules on the request body in the middleware
const addVaccineSummaryRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const addVaccineSummaryValidation = () => {
  return async (req, res, next) => {
    req.session.info = null
    const metaDataRepo = (await metaRepository()).repository
    const metaData = await metaDataRepo.fetch(req.cookies.sessionID)
    const errors = validationResult(req)
    if (req.body.addAnotherVaccine === undefined) {
     errors.errors.push({
       msg: 'common.errors.E0016',
       param: 'yes'
     })
    } else if(req.body.addAnotherVaccine === 'No' && metaData.DisplayedVaccineCount === 0){
        errors.errors.push({
       msg: 'common.errors.E0036',
            param: 'yes'
        })
    }

if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (!errors.isEmpty()) {
     // heres where you send the errors or do whatever you please with the error, in  your case
     res.redirect('add-vaccine-summary')
     return
    }
    // if everything went well, and all rules were passed, then move on to the next middleware
           if(req.session.errors){
if(!req.session.errors.length){
            req.session.errors = null
        }
        }
    next()
  }
}
module.exports = {
  addVaccineSummaryRules,
  addVaccineSummaryValidation
}
