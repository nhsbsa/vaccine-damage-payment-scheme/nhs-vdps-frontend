const { check, validationResult } = require('express-validator')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const addVaccineQuestionRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const addVaccineQuestionValidation = () => {
  return async (req, res, next) => {
   // execute the rules
   const errors = validationResult(req)
   if (req.body.vaccineReceived === undefined) {
     errors.errors.push({
       msg: 'common.errors.E0035',
       param: 'Coronavirus%20(COVID-19)',
       section: 'vaccineReceived'
     })
   }
    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
   // check if any of the request body failed the requirements set in validation rules
    if(!errors.isEmpty()){
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)


        applicationStatus.VaccinesReceived = 'ToDo'

        await applicationStatusRepo.save(applicationStatus)
    }
   if (!errors.isEmpty()) {
     // heres where you send the errors or do whatever you please with the error, in  your case
        res.redirect('add-vaccine')
     return
   }
   delete req.session.errors;

   next()
  }
}
module.exports = {
  addVaccineQuestionRules,
  addVaccineQuestionValidation
}
