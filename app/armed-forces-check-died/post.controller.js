'use strict'
const vacRepository = require('../../common/schemas/Vaccinations')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const metaRepository = require('../../common/schemas/MetaData')

module.exports = () => {
    return {
        render: async (req, res) => {

            if(req.body.armForcesCheck === 'Yes'){
                res.redirect('add-vaccine-details')
            } else {
                const metaRepo = (await metaRepository()).repository
                const metaData = await metaRepo.fetch(req.cookies.sessionID)
                let count = metaData.NumberOfVaccines;

                const repo = (await vacRepository()).repository
                const data = await repo.fetch(req.cookies.sessionID+':'+ count)
                data.DisplayVaccination = false
                await repo.save(data)
                res.redirect('claim-rejected')
            }
        }
    }
}
