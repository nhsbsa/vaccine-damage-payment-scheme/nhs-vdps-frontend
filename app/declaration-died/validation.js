const { check, validationResult } = require('express-validator')
const dataRepository = require('../../common/schemas/Data')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const declarationRules = () => {
  return [
        check('declarationA').isIn(['declarationA']).withMessage('common.errors.E0037')
  ]
}

// while this function executes the actual rules against the request body
const declarationValidation = () => {

    return async (req, res, next) => {

        const dataRepo = (await dataRepository()).repository
        const data = await dataRepo.fetch(req.cookies.sessionID)

        // execute the rules
        const errors = validationResult(req)
        let route = req.route.path;
        if(data.ApplyingFor === 'Deceased'){
            route = '/declaration-died'
        }else{
            route = '/declaration'
        }
        req.session.errors = errors.errors.map(err => Object.assign({},err,{route: route}))
        if(!errors.isEmpty()){
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)


            applicationStatus.Declaration = 'ToDo'

        }
        if (errors.isEmpty()) {
            if(req.session.errors){
                req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            }
            if(!req.session.errors){
                if(!req.session.errors.length){
                req.session.errors = null
            }
        }
       applicationStatus.Declaration = 'Completed'
 }
        next()
    }
}
module.exports = {
  declarationRules,
  declarationValidation
}
