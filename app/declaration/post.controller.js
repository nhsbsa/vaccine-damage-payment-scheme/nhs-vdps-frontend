'use strict'
const claimRepository = require('../../common/schemas/Claim')
const dataRepository = require('../../common/schemas/Data')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')

module.exports = () => {
    return {
        render: async (req, res) => {
            req.session.section = 'Declaration'

            const claimRepo = (await claimRepository()).repository
            const claimData = await claimRepo.fetch(req.cookies.sessionID)

            const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)

            claimData.DeclarationFirstName = req.body.declarationFirstName
            claimData.DeclarationLastName = req.body.declarationLastName

            if(req.body.declarationA){
                claimData.DeclarationA = true
            } else{
                claimData.DeclarationA = false
            }

            claimRepo.save(claimData)

            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                if(data.ApplyingFor === 'Deceased'){
                    res.redirect('declaration-died')
                } else if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
                    res.redirect('declaration-other')
                }
                else {
                    res.redirect('declaration')
                }
              return
            }

            res.redirect('upload-documents')
        }
    }
}
