const { req, res } = require('express');
const postController = require('./post.controller');
const claimRepository = require('../../common/schemas/Claim');
const dataRepository = require('../../common/schemas/Data');

jest.mock('../../common/schemas/Claim');
jest.mock('../../common/schemas/Data');

describe('Post Controller', () => {
    let claimRepo, dataRepo;

    beforeEach(async () => {
        claimRepo = {
            fetch: jest.fn().mockResolvedValue({}),
            save: jest.fn()
        };
        
        dataRepo = { 
            fetch: jest.fn().mockResolvedValue({}) 
        };

        claimRepository.mockResolvedValue({ repository: claimRepo });
        dataRepository.mockResolvedValue({ repository: dataRepo });
    });

    test('should set current section in session', async () => {
        await postController().render(req, res);
        expect(req.session.section).toBe('Declaration');
    });

    test('should update claim data with request body data', async () => {
        req.body = {
            declarationFirstName: 'Ryan',
            declarationLastName: 'Hughes',
            declarationA: 'checked'
        };

        const claimData = {};
        claimRepo.fetch.mockResolvedValue(claimData);

        await postController().render(req, res);

        expect(claimData.DeclarationFirstName).toBe('Ryan');
        expect(claimData.DeclarationLastName).toBe('Hughes');
        expect(claimData.DeclarationA).toBe(true);
        expect(claimRepo.save).toHaveBeenCalledWith(claimData);
    });

    test('should handle redirection for deceased claimants', async () => {
        req.session.errors = [{ route: '/path' }];
        dataRepo.fetch.mockResolvedValue({ ApplyingFor: 'Deceased' });
        await postController().render(req, res);
        expect(res.redirect).toHaveBeenCalledWith('declaration-died');
    });

    test('should handle redirection for claimants under 16', async () => {
        req.session.errors = [{ route: '/path' }];
        dataRepo.fetch.mockResolvedValue({ ApplyingFor: 'Under16' });
        await postController().render(req, res);
        expect(res.redirect).toHaveBeenCalledWith('declaration-other');
    });

    test('should handle redirection for disabled claimants', async () => {
        req.session.errors = [{ route: '/path' }];
        dataRepo.fetch.mockResolvedValue({ ApplyingFor: 'Disabled' });
        await postController().render(req, res);
        expect(res.redirect).toHaveBeenCalledWith('declaration-other');
    });

    test('should handle redirection for other claimants', async () => {
        req.session.errors = [{ route: '/path' }];
        dataRepo.fetch.mockResolvedValue({ ApplyingFor: 'Other' });
        await postController().render(req, res);
        expect(res.redirect).toHaveBeenCalledWith('declaration');
    });

    test('should redirect to upload-documents when there are no errors', async () => {
        req.session.errors = null;
        await postController().render(req, res);
        expect(res.redirect).toHaveBeenCalledWith('upload-documents');
    });
});
