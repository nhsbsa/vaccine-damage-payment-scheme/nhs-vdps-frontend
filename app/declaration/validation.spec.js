const { validationResult } = require('express-validator');
const { declarationRules } = require('./validation');

describe('declarationRules', () => {

    const runValidations = async (req) => {
        await Promise.all(declarationRules().map(validation => validation.run(req)));
        return validationResult(req);
    };

    test('should pass validation for first and last names and agreement checkbox', async () => {
        req = { body: { declarationFirstName: 'Ryan', declarationLastName: 'Hughes', declarationA: 'declarationA' }};
        const result = await runValidations(req);
        expect(result.isEmpty()).toBe(true);
    });

    test.each([
        undefined, null, "", "x"
    ])('should fail validation when first name is missing or too short', async (name) => {
        req = { body: { declarationFirstName: name, declarationLastName: 'Hughes', declarationA: 'declarationA' }};
        const result = await runValidations(req);
        expect(result.errors.length).toBe(1);
        expect(result.errors[0].msg).toBe('common.errors.E0024');
    });

    test('should fail validation when first name is 101 characters', async () => {
        req = { body: { 
            declarationFirstName: 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvw', 
            declarationLastName: 'Hughes', declarationA: 'declarationA' 
        }};
        const result = await runValidations(req);
        expect(result.errors.length).toBe(1);
        expect(result.errors[0].msg).toBe('common.errors.E0025');
    });

    test.each([
        "Ryan123", "Ryan!", "Ryan_", "Ryan$", "Ryan@", "Ryan/" // etc
    ])('should fail validation when first name contains invalid characters', async (name) => {
        req = { body: { declarationFirstName: name, declarationLastName: 'Hughes', declarationA: 'declarationA' }};
        const result = await runValidations(req);
        expect(result.errors.length).toBe(1);
        expect(result.errors[0].msg).toBe('common.errors.E0026');
    });

    test.each([
        undefined, null, "", "x"
    ])('should fail validation when last name is missing or too short', async (name) => {
        req = { body: { declarationFirstName: "Ryan", declarationLastName: name, declarationA: 'declarationA' }};
        const result = await runValidations(req);
        expect(result.errors.length).toBe(1);
        expect(result.errors[0].msg).toBe('common.errors.E0030');
    });

    test('should fail validation when last name is 101 characters', async () => {
        req = { body: { 
            declarationFirstName: 'Ryan',
            declarationLastName: 'a'.repeat(101),
            declarationA: 'declarationA' 
        }};
        const result = await runValidations(req);
        expect(result.errors.length).toBe(1);
        expect(result.errors[0].msg).toBe('common.errors.E0031');
    });

    test.each([
        "Hughes123", "Hughes!", "Hughes_", "Hughes$", "Hughes@", "Hughes/" // etc
    ])('should fail validation when last name contains invalid characters', async (name) => {
        req = { body: { declarationFirstName: 'Ryan', declarationLastName: name, declarationA: 'declarationA' }};
        const result = await runValidations(req);
        expect(result.errors.length).toBe(1);
        expect(result.errors[0].msg).toBe('common.errors.E0032');
    });

    test.each([
        undefined, null, "", "checked"
    ])('should fail validation when agreement is missing or invalid', async (agreement) => {
        req = { body: { declarationFirstName: "Ryan", declarationLastName: "Hughes", declarationA: agreement }};
        const result = await runValidations(req);
        expect(result.errors.length).toBe(1);
        expect(result.errors[0].msg).toBe('common.errors.E0037');
    });

});
