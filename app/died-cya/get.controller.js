'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const metaRepository = require('../../common/schemas/MetaData')
const dataRepository = require('../../common/schemas/Data')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const claimRepository = require('../../common/schemas/Claim')
const querystring = require('node:querystring'); 
const htmlentities = require('../../common/assets/htmlentities.json')

module.exports = () => {
  return {
    render: async (req, res) => {
      try {
        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)

        const metaRepo = (await metaRepository()).repository
        const metadata = await metaRepo.fetch(req.cookies.sessionID)

        const dpRepo = (await dpRepository()).repository
        const dp = await dpRepo.fetch(req.cookies.sessionID)

        const claimRepo = (await claimRepository()).repository
        const claim = await claimRepo.fetch(req.cookies.sessionID)

        if(!data.AccountVerified || !data.WhyMakingClaim ){
            res.redirect('/')
            return
        }

        let displayedAddress = dp.DpAddressLine1 + ', ' + dp.DpAddressLine2 + ', ' +  dp.DpTownOrCity + ', ' +  dp.DpCounty + ', ' + dp.DpPostcode
        displayedAddress = displayedAddress.replace('null, ','')
        displayedAddress = displayedAddress.replace(', ,',', ')
        dp.DisplayedAddress = displayedAddress


        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
        applicationStatus.DisabledPersonDetails = 'Completed'
         if(data.WhyMakingClaim === 'mother' || data.WhyMakingClaim === 'polio'){
            if(applicationStatus.VaccinatedPersonDetails !== 'Completed'){
                applicationStatus.VaccinatedPersonDetails = 'ToDo'
            }
        } else {
            if(applicationStatus.VaccinesReceived !== 'Completed'){
                applicationStatus.VaccinesReceived = 'ToDo'
            }
        }

        if(applicationStatus.VaccinesReceived !== 'Completed'){
            applicationStatus.VaccinesReceived = 'ToDo'
        }
        await applicationStatusRepo.save(applicationStatus)


        const templateBody = findFile('died-cya')
        let pageTitle;
        let md = templateBody.toString();
        if(md.includes('{#<pagetitle>')){
            pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
            md = md.split('</pagetitle>#}')[1]
        }

        let errors = null
        if(req.session.errors)
        {
            if(req.session.errors.filter(e => e.route === req.route.path).length){
                errors = req.session.errors.filter(e => e.route === req.route.path)
            }
        }
        
        const markdownRender = markdown(templateBody.toString())
        const params = {
          content: markdownRender,
          file: `./app/died-cya/templates/index.njk`,
          errors: errors,
          data: {
            DisabledPersonDetails: dp,
            MetaData: metadata,
            Data: data,
            Claim: claim
          },
          title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
        }
        res.render('app/index/index', params)
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })

        return res.redirect('back')
      }
    }
  }
}
