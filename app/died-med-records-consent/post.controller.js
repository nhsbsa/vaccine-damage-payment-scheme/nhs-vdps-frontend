'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
module.exports = () => {
  return {
    render: async (req, res) => {
        if(!req.body){
            res.redirect('died-med-records-consent')
        }

      try {
        return res.redirect('other-previous-claim-question')
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        return res.redirect('back')
      }
    }
  }
}
