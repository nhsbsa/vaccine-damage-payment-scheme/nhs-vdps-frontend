'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const dataRepository = require('../../common/schemas/Data')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const querystring = require('node:querystring');

module.exports = () => {
  return {
    render: async (req, res) => {
        try {
              const repo = (await dpRepository()).repository
              const dp = await repo.fetch(req.cookies.sessionID)
              dp.DpTitle = req.body.DpTitle
              dp.DpSurname = req.body.DpSurname
              dp.DpMiddlename = req.body.DpMiddlename ? req.body.DpMiddlename : null
              dp.DpForename = req.body.DpForename
              await repo.save(dp)
               if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                  res.redirect('died-name')
                  return
                }

                const dataRepo = (await dataRepository()).repository
                const data = await dataRepo.fetch(req.cookies.sessionID)


                res.redirect('died-dob')

                return
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.message
              })
            }
          res.redirect('died-name')
          return

    }
  }
}
