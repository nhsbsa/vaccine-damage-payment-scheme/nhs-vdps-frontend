const { check, validationResult } = require('express-validator')
const dataRepository = require('../../common/schemas/Data')
const { logRequestStart } = require('../../common/utils/request-logger')

// this sets the rules on the request body in the middleware
const diedNHSNumberRules = () => {
  return [
    /*check('NhsNumber')
        .trim()
        .optional({ checkFalsy: true, nullable: true })
        .matches(/^[0-9]+$/)
        .withMessage('common.errors.E0033')
        .bail()
        .isLength({ min: 10, max: 10 })
        .withMessage('common.errors.E0034')
        .escape()*/
  ]
}

// while this function executes the actual rules against the request body
const diedNHSNumberValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    req.session.errors = null
    const errors = validationResult(req)

    const nhsNumber = req.body.NhsNumber

    if (errors.errors.length === 0) {
      if (nhsNumber.trim().length > 0){
        if (!validateLengthString(nhsNumber)) {
          errors.errors.push({
            msg: 'common.errors.E0034',
            param: 'NhsNumber',
            formgroup: 'nhsnumber'
          })
        }
        if (!validateNumberString(nhsNumber)) {
          errors.errors.push({
            msg: 'common.errors.E0033',
            param: 'NhsNumber',
            formgroup: 'nhsnumber'
          })
        }
      }
    }

    if(req.session.errors){
        req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
    }else{
      req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }

    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
        // here where you send the errors or do whatever you please with the error, in  your case
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors.length){
            req.session.errors = null
        }
    }
    // if everything went well, and all rules were passed, then move on to the next middleware
    next()
  }
}

function validateNumberString (text) {
  const str = text.trim().replace(/ /g, '')
  return typeof(str) !== 'number' && !isNaN(str)
}

function validateLengthString (text) {
  const str = text.trim().replaceAll(" ","")
  return !(str.length < 10 || str.length > 10)
}

module.exports = {
  diedNHSNumberValidation,
  diedNHSNumberRules
}
