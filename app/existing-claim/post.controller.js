'use strict'
const metaRepository  = require('../../common/schemas/MetaData')
const { sendVerificationCode, notifyClient } = require('../../common/utils/claim')
const { logRequestStart } = require('../../common/utils/request-logger')
;
module.exports = () => {
  return {
    render: async (req, res) => {
        if(!req.body){
            res.redirect('existing-claim')
        }

      try {
        const metaRepo = (await metaRepository()).repository
        const meta = await metaRepo.fetch(req.cookies.sessionID)
        meta.Email = req.body.Email
        const id = await metaRepo.save(meta)
        res.cookie('data',id, { maxAge: 900000, httpOnly: true });
        try {
          const verification = await sendVerificationCode(meta.Email)
          if (verification.status === 201) {
            try {
              const expirationTime = Date.now() + 30 * 60000
              const date = new Date(expirationTime)
              const expiryDate = date.toString().substring(0, 15)
              const expiryTime = date.toString().substring(16, 24)
              req.session.authCode = verification.data.items.code

              //TODO REMOVE THIS - ADDED FOR PERF TESTING
                      req.session.authCode = verification.data.items.code

                      const notify = await notifyClient(meta.Email, verification.data.items.code, expiryDate, expiryTime, verification.data.items.tokval, 'existing')

              if (notify.status === 201) {
                   if(req.session.errors){
                        if(!req.session.errors.length){
                            req.session.errors = null
                        }
                    }
                return res.redirect('verification-code')
              } else {
                logRequestStart({
                  level: 'error',
                  message: "Error sending calling notifyClient"
                })
              }
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.response.data.errors.map(error => error.message).join('; ')
              })
              req.session.errors = e.response.data.errors.map(error => (
              {
                param: 'Email',
                msg: error.message,
                route: req.route.path,
                value: meta.Email,
                location: 'body'
              }
            ))
            res.redirect('existing-claim')
            return
            }
          }
        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })
          req.session.errors = [
            {
              param: 'Email',
              msg: e.message,
              route: req.route.path,
              value: meta.Email,
              location: 'body'
            }
          ]
          res.redirect('existing-claim')
          return
        }
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
        res.redirect('existing-claim')
       return
     }
    }
  }
}
