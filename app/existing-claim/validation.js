const { check, validationResult } = require('express-validator')

// this sets the rules on the request body in the middleware
const existingClaimRules = () => {
  return [
    check('Email')
        .trim()
        .isLength({ min: 7 })
        .withMessage('common.errors.E0046')
        .isLength({ max: 255 })
        .withMessage('common.errors.E0047')
        .bail()
        .isEmail()
        .withMessage('common.errors.E0048')
  ]
}

// while this function executes the actual rules against the request body
const existingClaimValidation = () => {
  return (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)

if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (!errors.isEmpty()) {
      // heres where you send the errors or do whatever you please with the error, in  your case
      res.redirect('existing-claim')
      return
    }
    // if everything went well, and all rules were passed, then move on to the next middleware
    next()
  }
}
module.exports = {
  existingClaimValidation,
  existingClaimRules
}
