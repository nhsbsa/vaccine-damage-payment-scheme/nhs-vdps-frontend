'use strict'
const dataRepository = require('../../common/schemas/Data')
const gpRepository = require('../../common/schemas/GeneralPractice')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const querystring = require('node:querystring');

module.exports = () => {
  return {
    render: async (req, res) => {
        try{
            req.session.section = 'DoctorDetails'
            const gpRepo = (await gpRepository()).repository
            const gp = await gpRepo.fetch(req.cookies.sessionID)

            gp.GpDoctorName = req.body.gpName
            gp.GpName = req.body.gpPracticeName
            gp.GpAddressLine1 = req.body.gpAddressLine1
            gp.GpAddressLine2 = req.body.gpAddressLine2 ? req.body.gpAddressLine2 : null
            gp.GpTownOrCity = req.body.gpTownOrCity
            gp.GpCounty = req.body.gpCounty ? req.body.gpCounty : null
            gp.GpPostcode = req.body.gpPostcode
            gp.GpTelNo = req.body.gpPhoneNumber ? req.body.gpPhoneNumber : null
            await gpRepo.save(gp)

            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
            applicationStatus.DoctorDetails = 'Completed'
            if(applicationStatus.HospitalAttendance !== 'Completed'){
                applicationStatus.HospitalAttendance = 'ToDo'
            }
            await applicationStatusRepo.save(applicationStatus)

            const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)

            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('gp-details-died')
                return
            }
            if(data.ApplyingFor === 'Deceased'){
                res.redirect('visited-hospital-died')
            } else {
                res.redirect('visited-hospital')
            }
            return
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }
        //res.redirect('gp-details-died')
        return
        //res.redirect('about-you-contact-details')

    }
  }
}
