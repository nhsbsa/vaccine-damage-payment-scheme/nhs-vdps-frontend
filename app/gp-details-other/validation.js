const { check, validationResult } = require('express-validator')
const dataRepository = require('../../common/schemas/Data')
const gpRepository = require('../../common/schemas/GeneralPractice')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const gpDetailsOtherRules = () => {
  return [
      check('gpName')
            .trim()
            .isLength({min: 2})
            .withMessage('common.errors.E0049')
            .isLength({max: 100})
            .withMessage('common.errors.E0050')
            .bail()
            .matches(/^[-A-Za-z /']+$/)
            .withMessage('common.errors.E0051'),
        check('gpPhoneNumber')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({min: 11})
            .withMessage('common.errors.E0052')
            .bail()
            .isLength({max: 25})
            .withMessage('common.errors.E0053')
            .bail()
            .matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)
            .withMessage('common.errors.E0054'),
        check('gpPracticeName')
            .trim()
            .isLength({min: 2})
            .withMessage('common.errors.E0055')
            .isLength({max: 100})
            .withMessage('common.errors.E0056')
            .bail()
            .matches(/^([a-zA-Z0-9 \-,:&'\/])*$/)
            .withMessage('common.errors.E0057'),
        check('gpAddressLine1')
            .trim()
            .isLength({min:1, max: 100})
            .withMessage('common.errors.E0001')
            .bail()
            .matches(/^([a-zA-Z0-9 \-,:'\/])*$/)
            .withMessage('common.errors.E0002'),
        check('gpAddressLine2')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({min:1, max: 100})
            .withMessage('common.errors.E0003')
            .bail()
            .matches(/^([a-zA-Z0-9 \-,:'\/])*$/)
            .withMessage('common.errors.E0004'),
        check('gpTownOrCity')
            .trim()
            .isLength({min: 2})
            .withMessage('common.errors.E0005')
            .isLength({max: 100})
            .withMessage('common.errors.E0006')
            .bail()
            .matches(/^[A-Za-z -']+$/)
            .withMessage('common.errors.E0062'),
        check('gpCounty')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({min: 3})
            .withMessage('common.errors.E0008')
            .isLength({max: 100})
            .withMessage('common.errors.E0009')
            .bail()
            .matches(/^[A-Za-z -']+$/)
            .withMessage('common.errors.E0010'),
        check('gpPostcode')
            .trim()
            .isLength({min: 5})
            .withMessage('common.errors.E0064')
            .isLength({max: 8})
            .withMessage('common.errors.E0065')
            .bail()
            .matches(/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})$/)
            .withMessage('common.errors.E0011')          
  ]
}

// while this function executes the actual rules against the request body
const gpDetailsOtherValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)

if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }



    if(!errors.isEmpty()){
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)


        applicationStatus.DoctorDetails = 'ToDo'

        await applicationStatusRepo.save(applicationStatus)
    }


    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
            req.session.errors = null
        }
        }
      // heres where you send the errors or do whatever you please with the error, in  your case

    }
    // if everything went well, and all rules were passed, then move on to the next middleware

    next()
  }
}
module.exports = {
  gpDetailsOtherValidation,
  gpDetailsOtherRules
}
