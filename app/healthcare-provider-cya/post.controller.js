'use strict'
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')

module.exports = () => {
  return {
    render: async (req, res) => {
                req.session.section = 'HealthClinicAttendance'
       if (req.body.addAnotherHealthcareProvider === 'Yes') {
            res.redirect('healthcare-provider-details')
        } else {
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
            applicationStatus.HealthClinicAttendance = 'Completed'
            if(applicationStatus.Declaration !== 'Completed'){
                applicationStatus.Declaration = 'ToDo'
            }
            await applicationStatusRepo.save(applicationStatus)

            res.redirect('medical-cya')
        }
    }
  }
}
