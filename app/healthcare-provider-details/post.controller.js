'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const healthcareProviderRepository = require('../../common/schemas/HealthcareProviders')
const metaRepository = require('../../common/schemas/MetaData')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
;
module.exports = () => {
  return {
    render: async (req, res) => {
    req.session.section = 'HealthClinicAttendance'
 try{
            const metaRepo = (await metaRepository()).repository
            const metaData = await metaRepo.fetch(req.cookies.sessionID)

            let count
            if(req.body.healthcareProviderCount){
                count =  Number(req.body.healthcareProviderCount)
            } else {
                count = metaData.NumberOfHealthcareProviders;
                if(!count){
                    count = 1
                } else {
                    count = count+1
                }
            }

            const dataRepo = (await healthcareProviderRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID + ':' + count)
            data.ClinicName = req.body.clinicName
            data.ClinicAddressLine1 = req.body.clinicAddressLine1
            data.ClinicAddressLine2 = req.body.clinicAddressLine2 ? req.body.clinicAddressLine2 : null
            data.ClinicTownOrCity = req.body.clinicTownOrCity
            data.ClinicCounty = req.body.clinicCounty ? req.body.clinicCounty : null
            data.ClinicPostcode = req.body.clinicPostcode
            data.ClinicConsultant = req.body.clinicConsultant ? req.body.clinicConsultant : null
            data.ClinicConsultantTelNo = req.body.clinicConsultantTelNo ? req.body.clinicConsultantTelNo : null
            data.ClinicConsultantEmail = req.body.clinicConsultantEmail ? req.body.clinicConsultantEmail : null
            data.ClinicCount = count
            data.DisplayOnline = false
            await dataRepo.save(data)

            if(!req.body.healthcareProviderCount){
                metaData.NumberOfHealthcareProviders = count
                await metaRepo.save(metaData)
            }

            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('healthcare-provider-details?healthcareProviderCount='+count)
                return
            }
            data.DisplayOnline = true
            await dataRepo.save(data)
            res.redirect('healthcare-provider-cya')
            return
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }
        res.redirect('healthcare-provider-details')
        return
    }
  }
}
