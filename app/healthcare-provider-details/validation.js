const { check, validationResult } = require('express-validator')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')

// this sets the rules on the request body in the middleware
const healthcareProviderDetailsRules = () => {
    return [
        check('clinicName')
            .trim()
            .isLength({min: 2})
            .withMessage('common.errors.E0066')
            .isLength({max: 100})
            .withMessage('common.errors.E0067')
            .bail()
            .matches(/^([a-zA-Z0-9 \-,:&'\/])*$/)
            .withMessage('common.errors.E0068'),
        check('clinicAddressLine1')
            .trim()
            .isLength({min:1, max: 100})
            .withMessage('common.errors.E0001')
            .bail()
            .matches(/^([a-zA-Z0-9 \-,:'\/])*$/)
            .withMessage('common.errors.E0002'),
        check('clinicAddressLine2')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({min:1, max: 100})
            .withMessage('common.errors.E0003')
            .bail()
            .matches(/^([a-zA-Z0-9 \-,:'\/])*$/)
            .withMessage('common.errors.E0004'),
        check('clinicTownOrCity')
            .trim()
            .isLength({min: 2})
            .withMessage('common.errors.E0005')
            .isLength({max: 100})
            .withMessage('common.errors.E0006')
            .bail()
            .matches(/^[-a-zA-Z .']+$/)
            .withMessage('common.errors.E0069'),
        check('clinicCounty')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({min: 3})
            .withMessage('common.errors.E0008')
            .isLength({max: 100})
            .withMessage('common.errors.E0009')
            .bail()
            .matches(/^[-a-zA-Z .']+$/)
            .withMessage('common.errors.E0070'),
        check('clinicPostcode')
            .trim()
            .isLength({min: 5, max: 8})
            .withMessage('common.errors.E0011')
            .bail()
            .matches(/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})$/)
            .withMessage('common.errors.E0011'),
        check('clinicConsultant')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({min: 2})
            .withMessage('common.errors.E0071')
            .isLength({max: 100})
            .withMessage('common.errors.E0072')
            .bail()
            .matches(/^[A-Za-z --'']+$/)
            .withMessage('common.errors.E0073'),
        check('clinicConsultantTelNo')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({min: 11})
            .withMessage('common.errors.E0074')
            .isLength({max: 25})
            .withMessage('common.errors.E0075')
            .bail()
            .matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)
            .withMessage('common.errors.E0054'),
        check('clinicConsultantEmail')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({max: 255})
            .withMessage('common.errors.E0076')
            .isLength({min: 7})
            .withMessage('common.errors.E0077')
            .bail()
            .isEmail()
            .withMessage('common.errors.E0048')
    ]
}

// while this function executes the actual rules against the request body
const healthcareProviderDetailsValidation = () => {
  return async (req, res, next) => {
    // execute the rules
       const errors = validationResult(req)
if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }

        if(!errors.isEmpty()){
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)


            applicationStatus.HealthClinicAttendance = 'ToDo'

            await applicationStatusRepo.save(applicationStatus)
        }

       // check if any of the request body failed the requirements set in validation rules
       if (errors.isEmpty()) {
        if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
            req.session.errors = null
        }
        }
       }
       // move to next middleware
       next()
     }
}
module.exports = {
  healthcareProviderDetailsRules,
  healthcareProviderDetailsValidation
}
