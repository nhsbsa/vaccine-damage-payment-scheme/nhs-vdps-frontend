'use strict'
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const dataRepository = require('../../common/schemas/Data')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')

module.exports = () => {
  return {
    render: async (req, res) => {
        if (req.body.addAnotherHospital === 'Yes') {
            res.redirect('hospital-details')
        } else {
            const applicationStatusRepo = (await applicationStatusRepository()).repository
            const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
            const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)

            applicationStatus.HospitalAttendance = 'Completed'
            if(applicationStatus.HealthClinicAttendance !== 'Completed'){
                applicationStatus.HealthClinicAttendance = 'ToDo'
            }
            await applicationStatusRepo.save(applicationStatus)

            if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
                res.redirect('visited-healthcare-provider-other')
            } else
            if(data.ApplyingFor === 'Deceased'){
                res.redirect('visited-healthcare-provider-died')
            } else {
                res.redirect('visited-healthcare-provider')
            }
        }
    }
  }
}
