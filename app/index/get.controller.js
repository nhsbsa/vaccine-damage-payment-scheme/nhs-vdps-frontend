'use strict'
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const dataRepository = require('../../common/schemas/Data')
const claimRepository = require('../../common/schemas/Claim')
const metaRepository = require('../../common/schemas/MetaData')
const repRepository = require('../../common/schemas/Representative')
const vacRepository = require('../../common/schemas/Vaccinations')
const VaccinatedPersonDetails = require('../../common/schemas/VaccinatedPersonDetails')
const vnRepository = require('../../common/schemas/VaccineNotes')
const gpRepository = require('../../common/schemas/GeneralPractice')
const {secretManager} = require('../../common/utils/aws')
const hospitalRepository = require('../../common/schemas/Hospitals')
const healthcareProviderRepository = require('../../common/schemas/HealthcareProviders')
const documentsRepository = require('../../common/schemas/Documents')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const vpRepository = require('../../common/schemas/VaccinatedPersonDetails')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const ClaimantContactPreferences = require('../../common/schemas/ClaimantContactPreferences')
const ClaimDisablement = require('../../common/schemas/ClaimDisablement')
const DisabledPersonDetails = require('../../common/schemas/DisabledPersonDetails')
const Documents = require('../../common/schemas/Documents')
const client = require('../../common/schemas/redis-client')
const { logRequestStart } = require('../../common/utils/request-logger')
const htmlentities = require('../../common/assets/htmlentities.json')
var sanitizer = require('sanitizer')
var xss = require("xss");

const { I18n } = require('i18n')
module.exports = (page) => {
    const resetRedisCacheExpiry = async function(sessionID) {
        // 45 minutes
        const timeout = 45
        // initiate repos
        const cdRepo = (await cdRepository()).repository
        const dataRepo = (await dataRepository()).repository
        const claimRepo = (await claimRepository()).repository
        const metaRepo = (await metaRepository()).repository
        const repRepo = (await repRepository()).repository
        const vacRepo = (await vacRepository()).repository
        const hospitalRepo = (await hospitalRepository()).repository
        const hpRepo = (await healthcareProviderRepository()).repository
        const appRepo = (await applicationStatusRepository()).repository
        const ccpRepo = (await ClaimantContactPreferences()).repository
        const cdisableRepo = (await ClaimDisablement()).repository
        const dpRepo = (await DisabledPersonDetails()).repository
        const docuRepo = (await Documents()).repository
        const gpRepo = (await gpRepository()).repository
        const vnRepo = (await vnRepository()).repository
        const vpRepo = (await VaccinatedPersonDetails()).repository
        const docRepo = (await documentsRepository()).repository

        // set time to expire seconds
        const ttlInSeconds = 1 * timeout * 60

        // set expiry of each repo to expiry time
        await cdRepo.expire(sessionID, ttlInSeconds)
        await dataRepo.expire(sessionID, ttlInSeconds)
        await claimRepo.expire(sessionID, ttlInSeconds)
        await metaRepo.expire(sessionID, ttlInSeconds)
        await repRepo.expire(sessionID, ttlInSeconds)
        await vacRepo.expire(sessionID, ttlInSeconds)
        await hospitalRepo.expire(sessionID, ttlInSeconds)
        await hpRepo.expire(sessionID, ttlInSeconds)
        await appRepo.expire(sessionID, ttlInSeconds)
        await ccpRepo.expire(sessionID, ttlInSeconds)
        await cdisableRepo.expire(sessionID, ttlInSeconds)
        await dpRepo.expire(sessionID, ttlInSeconds)
        await docuRepo.expire(sessionID, ttlInSeconds)
        await gpRepo.expire(sessionID, ttlInSeconds)
        await vnRepo.expire(sessionID, ttlInSeconds)
        await vpRepo.expire(sessionID, ttlInSeconds)

        return
    }

    const checkTimeout = async function(req) {
        var time = (Date.now() / 1000) - (5 * 60); // Five minutes ago
        if((req.session.blocked - time) > 0) {
            logRequestStart({
                level: 'error',
                message: `User blocked for another ${req.session.blocked - time} seconds`
            })
        }else{
            req.session.blocked = null
        }

        return true
    }

    const getVaccineList = function(){
        let arr = [];
        for (let i = 0; i < 25; i++) {
            arr[i] = "common.vaccine.item" + [i+1]
        }
        return arr;
    }

    const getRelationshipList = function(){
        let arr = [];
        for (let i = 0; i < 25; i++) {
            arr[i] = "common.relationship.item" + [i+1]
        }
        return arr;
    }

    const checkSection = function(ApplicationStatus, req) {

        const route = req.route.path.replace('/','')

        const sectionsToCheck = ['YourDetails','NominatedPersonDetails','VaccinesReceived','DatesOfVaccination','WhatHappenedNext','DoctorDetails','HospitalAttendance','HealthClinicAttendance','Declaration']
        const section = req.session.section

        sectionsToCheck.length = section ? sectionsToCheck.indexOf(section) : 0
        const errors = [];
        for (const check of sectionsToCheck){
            if(ApplicationStatus[check] !== 'Completed'){
                errors.push(check)
            }
        }
//        console.log('checkSection',section)
//        console.log('sectionsToCheck',sectionsToCheck)
//        console.log('checkSection errors',errors)
//        console.log('errors.length',errors.length)
        if(errors.length){
            return false
        }

        return true
    }

  return {
    render: async (req, res) => {
 let pageTitle;
        const templateBody = findFile(page)
        let md = templateBody.toString();
        if(md.includes('{#<pagetitle>')){
            pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
            md = md.split('</pagetitle>#}')[1]
        }

        const markdownRender = markdown(md.replace(pageTitle,''))
        const Now = new Date()
        const SaveDate = new Date(Date.now() + 12096e5);

        const Info = req.session.info
        const Timeout = req.session.timeout


        let Vaccine
        let vaccine
        let fetchedVaccines = []
        let DisplayedVaccines = []
        let Hospital
        let HealthcareProvider
        let document
        let Documents = []
        let VaccineList = getVaccineList()
        let RelationshipList = getRelationshipList()

        // Set expiration timeout on schemas.
        try{
            await resetRedisCacheExpiry(req.sessionID)
        }catch(e){
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }

        if(req.query.lang) {
            res.cookie("lang", req.query.lang);
        }

//        if(req.session?.errors)
//            if(req.route.path !== req.session.errors[0]?.route)
//                if(!req.session.errors.length){


        if (!req.cookies){
            res.cookie('sessionID', req.sessionID)
            req.session.newVisitor = true
        } else {
            req.session.newVisitor = null
        }

        if (req.cookies && !req.cookies.sessionID){
            res.cookie('sessionID', req.sessionID)
            req.session.newVisitor = true
        } else {
            req.session.newVisitor = null
        }



        if(page === 'eligibility'){
            if(req.session.errors){
                if(!req.session.errors.length){
                    req.session.errors = null
                }
            }
        }

        const cdRepo = (await cdRepository()).repository
        const ClaimantDetails = (await cdRepo.fetch(req.cookies.sessionID)).toJSON()

        const dataRepo = (await dataRepository()).repository
        const Data = (await dataRepo.fetch(req.cookies.sessionID)).toJSON()

        const claimRepo = (await claimRepository()).repository
        const Claim = await claimRepo.fetch(req.cookies.sessionID)

        const metaRepo = (await metaRepository()).repository
        const MetaData = await metaRepo.fetch(req.cookies.sessionID)


        const repRepo = (await repRepository()).repository
        const Representative = await repRepo.fetch(req.cookies.sessionID)

        const vnRepo = (await vnRepository()).repository
        const VaccineNotes = await vnRepo.fetch(req.cookies.sessionID)

        const gpRepo = (await gpRepository()).repository
        const GeneralPractice = await gpRepo.fetch(req.cookies.sessionID)

        const appRepo = (await applicationStatusRepository()).repository
        const ApplicationStatus = (await appRepo.fetch(req.cookies.sessionID)).toJSON()

        //Vaccines
        const vacRepo = (await vacRepository()).repository
        const vacClient = (await vacRepository()).client

        if(req.route.path === '/done' || req.route.path === '/saved-claim'){
            let Email = MetaData.Email
            // TODO: PUT BACK IN
            req.session.destroy(function(err) {
                if (err) {
                    throw new Error(err);
                } else {
                    res.clearCookie();
                    const params = {
                        content: markdownRender,
                        file: page !== 'index' ? `./app/${page}/templates/index.njk` : `./app/index/templates/index.njk`,
                        errors: req.session?.errors || null,
                        locale: req.getLocale(),
                        data: {Now, SaveDate, Timeout, Info, MetaData:{ Email: Email}},
                        title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
                    }
                    res.render('app/index/index', params)
                }
            });
            return
        }

        if(req.route.path === '/medical-history-info'){
            req.session.section = 'VaccinesReceived'
        }
        if(req.route.path === '/gp-details' || req.route.path === '/gp-details-other' || req.route.path === '/gp-details-died'){
            req.session.section = 'DoctorDetails'
        }
        const introPages = ['/','/what-you-need','/user-auth','/user-verify','/existing-claim','/verification-code','/verification-dob','/version']

        if(!Data.AccountVerified && !introPages.includes(req.route.path)){
            res.redirect('/')
            return
        }

        //await checkTimeout(req)
        if(Data.AccountVerified && !await checkSection(ApplicationStatus, req)){
            res.redirect('/your-application')
            return
        }

        if(req.query.vaccineCount){
            Vaccine = await vacRepo.fetch(req.cookies.sessionID + ':' + req.query.vaccineCount)
        } else {
            Vaccine = await vacRepo.fetch(req.cookies.sessionID + ':' + MetaData.NumberOfVaccines)
        }


        for(let i=1; i<= MetaData.NumberOfVaccines; i++){
            vaccine = await vacRepo.fetch(req.cookies.sessionID + ":" + i)
            fetchedVaccines.push(vaccine)
        }

        let vaccineLocation

        for(const vaccine of fetchedVaccines){
            if(vaccine.DisplayVaccination){
                if(!vaccineLocation){
                    vaccineLocation = vaccine.VaccinationCountry
                }

                if(vaccine.VaccinationCountry === 'Outside United Kingdom' && vaccineLocation !== 'uk'){
                    vaccineLocation = 'armed forces'
                } else {
                    vaccineLocation = 'uk'
                }

                DisplayedVaccines.push(vaccine)
            }
        }

        Claim.Country = vaccineLocation ? vaccineLocation : null
        claimRepo.save(Claim)

        MetaData.DisplayedVaccineCount = DisplayedVaccines.length
        await metaRepo.save(MetaData)

        //Hospitals
        const hospitalRepo = (await hospitalRepository()).repository
        if(req.query.hospitalCount){
            Hospital = await hospitalRepo.fetch(req.cookies.sessionID + ':' + req.query.hospitalCount)
        }

        //Healthcare Provider
        const hpRepo = (await healthcareProviderRepository()).repository
        if(req.query.healthcareProviderCount){
            HealthcareProvider = await hpRepo.fetch(req.cookies.sessionID + ':' + req.query.healthcareProviderCount)
        }

        //Documents
        const docRepo = (await documentsRepository()).repository
        for(let i=1; i<= MetaData.NumberOfFiles; i++){
            document = await docRepo.fetch(req.cookies.sessionID + ":" + i)
            Documents.push(document)
        }

        //Disabled Person Details
        const dpRepo = (await dpRepository()).repository
        const DisabledPersonDetails = (await dpRepo.fetch(req.cookies.sessionID)).toJSON()

        //Vaccinated Person Details
        const vpRepo = (await vpRepository()).repository
        const VaccinatedPersonDetails = (await vpRepo.fetch(req.cookies.sessionID)).toJSON()


        let TempEmail;

        if(req.session.email){
             TempEmail = req.session.email
        }

        if(page === 'index'){
            if(req.session.errors){

                if(!req.session.errors.length){
                    req.session.errors = null
                }
            }else{
                req.session.errors = null
            }

            req.session.info = null
        }

        if(page !== 'add-vaccine-summary'){
            req.session.info = null
        }
        let errors = null
        if(req.session.errors)
        {
            if(req.session.errors.length){
                req.session.errors = Array.from(new Set(req.session.errors.filter(e => e).map(a => a.route+a.param)))
                                          .map(id => {
                                            return req.session.errors.filter(e => e).find(a => a.route+a.param === id)
                                          })
                if(req.session.errors.filter(e => e).filter(e => e.route === req.route.path).length){
                    errors = req.session.errors.filter(e => e).filter(e => e.route === req.route.path)
                }
            }
        }
        
        let data = {
                                 ClaimantDetails,
                                 Data,
                                 Claim,
                                 MetaData,
                                 Representative,
                                 TempEmail,
                                 Vaccine,
                                 DisplayedVaccines,
                                 VaccineNotes,
                                 GeneralPractice,
                                 Hospital,
                                 HealthcareProvider,
                                 Info,
                                 Timeout,
                                 Documents,
                                 DisabledPersonDetails,
                                 VaccinatedPersonDetails,
                                 Now,
                                 VaccineList,
                                 RelationshipList,
                                 SaveDate
                               }

        const params = {
            content: markdownRender,
            file: page !== 'index' ? `./app/${page}/templates/index.njk` : `./app/index/templates/index.njk`,
            errors: errors ? req.session?.errors.map(err => Object.assign({},err,{"locale": locale})).filter(e => e.route === req.route.path) : null || null,
            locale: req.getLocale(),
            data: data,
            title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
        }

        // A request has been redirected with user input body
        if(req.session.body){
            params.data.body = req.session.body
            delete req.session.body;
        }

        res.render('app/index/index', params)
        return
    }
  }
}
