'use strict'

// NPM dependencies
const expect = require('chai').expect
const supertest = require('supertest')

const { describe, it } = require('mocha')
const fs = require('fs')
// Local dependencies
const getApp = require('../../server').getApp
// Markdown folder where S3 files have been downloaded
const markdownFolder = 'common/templates/markdown'

// Read directory and loops files
// eslint-disable-next-line handle-callback-err
fs.readdir(markdownFolder, async (err, files) => {
  for (const file of files) {

    // Remove extension from filename
    const indexPath = file.split('.')[0]
    // Use filename as route unless index file then replace with root
    const paths = {
      index: (indexPath === 'index') ? '/' : `/${indexPath}`
    }
    describe(`GET ${paths.index} endpoint`, () => {
      it('should return HTTP 200 status with expected JSON', done => {
        supertest(getApp())
          .get(paths.index)
          .set('Accept', 'application/json')
          .expect(200)
          .expect(res => {
            const response = JSON.parse(res.text)
            expect(response.ping.healthy).to.equal(true)
          })
          .end(done)
      })
    })
  }
})
