
// Npm dependencies
const express = require('express')
const fs = require('fs')

// Local dependencies
const { logRequestStart } = require('../../common/utils/request-logger')

// Controllers
// Dynamic GET Controller
const getController = require('./get.controller')

// Auth Controllers
const ExistingClaim = require('../existing-claim/post.controller')
const UserAuth = require('../user-auth/post.controller')
const UserAuthResend = require('../user-auth/resend.get.controller')
const UserVerify = require('../user-verify/post.controller')
const VerificationCode = require('../verification-code/post.controller')
const VerificationDOB = require('../verification-dob/post.controller')
const SaveClaim = require('../save-claim/post.controller')
const StoreClaim = require('../store-claim/get.controller')

// Intro Controllers
const WhoAreYouApplyingFor = require('../who-are-you-applying-for/post.controller')
const MedRecordsConsent = require('../med-records-consent/post.controller')
const PreviousClaimQuestion = require('../previous-claim-question/post.controller')
const WhyAreYouMakingThisClaim = require('../why-are-you-making-this-claim/post.controller')
const DiedMedRecordsConsent = require('../died-med-records-consent/post.controller')
const OtherMedRecordsConsent = require('../other-med-records-consent/post.controller')
const OtherPreviousClaimQuestion = require('../other-previous-claim-question/post.controller')
const OtherWhyAreYouMakingThisClaim = require('../other-why-are-you-making-this-claim/post.controller')

// About You Controllers
const AboutYouName = require('../about-you-name/post.controller')
const AboutYouDOB = require('../about-you-dob/post.controller')
const AboutYouNHSNumber = require('../about-you-nhs-number/post.controller')
const AboutYouAddress = require('../about-you-address/post.controller')
const AboutYouContactDetails = require('../about-you-contact-details/post.controller')
const AboutYouContactPrefs = require('../about-you-contact-prefs/post.controller')
const AboutYouCYA = require('../about-you-cya/get.controller')
const OtherAboutYouCYA = require('../other-about-you-cya/get.controller')
const AccountNotVerifiedCYA = require('../account-not-verified/get.controller')
const AboutYouDiedRelationship = require('../about-you-died-relationship/post.controller')
const AboutYouUnder16Relationship = require('../about-you-under16-relationship/post.controller')

// Representative Controllers
const RepresentativeQuestion = require('../representative-question/post.controller')
const RepresentativeName = require('../representative-name/post.controller')
const RepresentativePos = require('../representative-pos/post.controller')
const RepresentativeAddress = require('../representative-address/post.controller')
const RepresentativeContact = require('../representative-contact/post.controller')
const RepresentativeCYA = require('../representative-cya/get.controller')

// Your Application
const YourApplication = require('../your-application/get.controller')

// Vaccine Controllers
const AddVaccine = require('../add-vaccine/post.controller')
const AddVaccineDied = require('../add-vaccine-died/post.controller')
const AddVaccineOther = require('../add-vaccine-other/post.controller')
const AddVaccineLocation = require('../add-vaccine-location/post.controller')
const ArmedForcesCheck = require('../armed-forces-check/post.controller')
const ArmedForcesCheckDied = require('../armed-forces-check-died/post.controller')
const AddVaccineDetails = require('../add-vaccine-details/post.controller')
const AddVaccineSummary = require('../add-vaccine-summary/post.controller')
const DeleteVaccine = require('../vaccines/delete.controller')
const VaccineNotes = require('../vaccine-notes/post.controller')
const addCovid19Type = require('../add-covid19-type/post.controller')
const addVaccineCountry = require('../add-vaccine-country/post.controller')
const vaccineDiagnosis = require('../vaccine-diagnosis/post.controller')

//GP Controllers
const GPDetails = require('../gp-details/post.controller')
const GPDetailsDied = require('../gp-details-died/post.controller')
const GPDetailsOther = require('../gp-details-other/post.controller')

//Hospital Controllers
const VisitHospital = require('../visited-hospital/post.controller')
const VisitHospitalDied = require('../visited-hospital-died/post.controller')
const VisitHospitalOther = require('../visited-hospital-other/post.controller')
const HospitalDetails = require('../hospital-details/post.controller')
const HospitalCYAGet = require('../hospital-cya/get.controller')
const HospitalCYAPost = require('../hospital-cya/post.controller')

//Healthcare Provider Controllers
const VisitHealthcareProvider = require('../visited-healthcare-provider/post.controller')
const HealthcareProviderDetails = require('../healthcare-provider-details/post.controller')
const HealthcareProviderCYAGet = require('../healthcare-provider-cya/get.controller')
const HealthcareProviderCYAPost = require('../healthcare-provider-cya/post.controller')

//Medical Controller
const MedicalCYA = require('../medical-cya/get.controller')

//Upload Documents Controllers
const UploadDocuments = require('../upload-documents/post.controller')
const DeleteDocument = require('../delete-document/post.controller')

//Declaration Controller
const Declaration = require('../declaration/post.controller')

//Died
const DiedName = require('../died-name/post.controller')
const DiedDOB = require('../died-dob/post.controller')
const DiedDOD = require('../died-dod/post.controller')
const DiedNHSNumber = require('../died-nhs-number/post.controller')
const DiedAddress = require('../died-address/post.controller')
const DiedCYA = require('../died-cya/get.controller')

//Other
const OtherName = require('../other-name/post.controller')
const OtherDOB = require('../other-dob/post.controller')
const OtherNHSNumber = require('../other-nhs-number/post.controller')
const OtherAddress = require('../other-address/post.controller')
const OtherCYA = require('../other-cya/get.controller')

//Mother
const MothersName = require('../mothers-name/post.controller')
const MothersDOB = require('../mothers-dob/post.controller')
const MothersCYA = require('../mothers-cya/get.controller')
const OtherMothersName = require('../other-mothers-name/post.controller')
const OtherMothersDOB = require('../other-mothers-dob/post.controller')
const OtherMothersCYA = require('../other-mothers-cya/get.controller')

//Polio
const PolioName = require('../polio-name/post.controller')
const PolioDOB = require('../polio-dob/post.controller')
const PolioCYA = require('../polio-cya/get.controller')
const PolioDates = require('../polio-dates/post.controller')
const PolioNotes = require('../polio-notes/post.controller')

// Validation
// Auth Validation
const { existingClaimRules, existingClaimValidation } = require('../existing-claim/validation.js')
const { userAuthRules, userAuthValidation } = require('../user-auth/validation.js')
const { userVerifyRules, userVerifyValidation } = require('../user-verify/validation.js')
const { verificationRules, verificationValidation } = require('../verification-code/validation.js')
const { verificationDOBRules, verificationDOBValidation } = require('../verification-dob/validation.js')
const { SaveClaimRules, SaveClaimValidation } = require('../save-claim/validation.js')
const { StoreClaimRules, StoreClaimValidation } = require('../store-claim/validation.js')

// Intro Vadliation
const { whoAreYouApplyingForRules, whoAreYouApplyingForValidation } = require('../who-are-you-applying-for/validation.js')
const { medRecordsConsentRules, medRecordsConsentValidation } = require('../med-records-consent/validation.js')
const { previousClaimQuestionRules, previousClaimQuestionValidation } = require('../previous-claim-question/validation.js')
const { otherPreviousClaimQuestionRules, otherPreviousClaimQuestionValidation } = require('../other-previous-claim-question/validation.js')
const { whyAreYouMakingThisClaimRules, whyAreYouMakingThisClaimValidation } = require('../why-are-you-making-this-claim/validation.js')
const { diedMedRecordsConsentRules, diedMedRecordsConsentValidation } = require('../died-med-records-consent/validation.js')
const { otherWhyAreYouMakingThisClaimRules, otherWhyAreYouMakingThisClaimValidation } = require('../other-why-are-you-making-this-claim/validation.js')
const { otherMedRecordsConsentRules, otherMedRecordsConsentValidation } = require('../other-med-records-consent/validation.js')

// About You Name Validation
const { aboutYouNameRules, aboutYouNameValidation } = require('../about-you-name/validation.js')
const { aboutYouDOBRules, aboutYouDOBValidation } = require('../about-you-dob/validation.js')
const { aboutYouNHSNumberRules, aboutYouNHSNumberValidation } = require('../about-you-nhs-number/validation.js')
const { aboutYouAddressRules, aboutYouAddressValidation } = require('../about-you-address/validation.js')
const { aboutYouContactDetailsRules, aboutYouContactDetailsValidation } = require('../about-you-contact-details/validation.js')
const { aboutYouContactPrefsRules, aboutYouContactPrefsValidation } = require('../about-you-contact-prefs/validation.js')
const { aboutYouDiedRelationshipRules, aboutYouDiedRelationshipValidation } = require('../about-you-died-relationship/validation.js')
const { aboutYouUnder16RelationshipRules, aboutYouUnder16RelationshipValidation } = require('../about-you-under16-relationship/validation.js')

// Representative
const { representativeQuestionRules, representativeQuestionValidation } = require('../representative-question/validation.js')
const { representativeNameRules, representativeNameValidation } = require('../representative-name/validation.js')
const { representativePosRules, representativePosValidation } = require('../representative-pos/validation.js')
const { representativeAddressRules, representativeAddressValidation } = require('../representative-address/validation.js')
const { representativeContactRules, representativeContactValidation } = require('../representative-contact/validation.js')

// Vaccine
const { addVaccineQuestionRules, addVaccineQuestionValidation } = require('../add-vaccine/validation.js')
const { addVaccineDiedQuestionRules, addVaccineDiedQuestionValidation } = require('../add-vaccine-died/validation.js')
const { addVaccineOtherQuestionRules, addVaccineOtherQuestionValidation } = require('../add-vaccine-other/validation.js')
const { addVaccineLocationQuestionRules, addVaccineLocationValidation } = require('../add-vaccine-location/validation.js')
const { armedForcesCheckRules, armedForcesCheckValidation } = require('../armed-forces-check/validation.js')
const { armedForcesCheckDiedRules, armedForcesCheckDiedValidation } = require('../armed-forces-check-died/validation.js')
const { addVaccineDetailsRules, addVaccineDetailsValidation } = require('../add-vaccine-details/validation.js')
const { addVaccineSummaryRules, addVaccineSummaryValidation } = require('../add-vaccine-summary/validation.js')
const { vaccineNotesRules, vaccineNotesValidation } = require('../vaccine-notes/validation.js')
const { addCovid19TypeValidation } = require('../add-covid19-type/validation.js')
const { addVaccineCountryValidation } = require('../add-vaccine-country/validation.js')
const { vaccineDiagnosisValidation } = require('../vaccine-diagnosis/validation.js')

// GP Details
const { gpDetailsRules, gpDetailsValidation } = require('../gp-details/validation.js')
const { gpDetailsDiedRules, gpDetailsDiedValidation } = require('../gp-details-died/validation.js')
const { gpDetailsOtherRules, gpDetailsOtherValidation } = require('../gp-details-other/validation.js')

// Hospital
const { visitedHospitalRules, visitedHospitalValidation } = require('../visited-hospital/validation.js')
const { visitedHospitalDiedRules, visitedHospitalDiedValidation } = require('../visited-hospital-died/validation.js')
const { visitedHospitalOtherRules, visitedHospitalOtherValidation } = require('../visited-hospital-other/validation.js')
const {hospitalDetailsRules, hospitalDetailsValidation} = require('../hospital-details/validation.js')
const {hospitalCYARules, hospitalCYAValidation} = require('../hospital-cya/validation.js')

// Healthcare Providers
const { visitedHealthcareProviderRules, visitedHealthcareProviderValidation } = require('../visited-healthcare-provider/validation.js')
const {healthcareProviderDetailsRules, healthcareProviderDetailsValidation} = require('../healthcare-provider-details/validation.js')
const {healthcareProviderCYARules, healthcareProviderCYAValidation} = require('../healthcare-provider-cya/validation.js')

//Upload Documents
const { uploadDocumentsRules, uploadDocumentsValidation } = require('../upload-documents/validation.js')
const { deleteDocumentRules, deleteDocumentValidation } = require('../delete-document/validation.js')
//Declaration
const { declarationRules, declarationValidation } = require('../declaration/validation.js')

// About You Name Validation
const { diedNameRules, diedNameValidation } = require('../died-name/validation.js')
const { diedDOBRules, diedDOBValidation } = require('../died-dob/validation.js')
const { diedDODRules, diedDODValidation } = require('../died-dod/validation.js')
const { diedNHSNumberRules, diedNHSNumberValidation } = require('../died-nhs-number/validation.js')
const { diedAddressRules, diedAddressValidation } = require('../died-address/validation.js')

// Other Name Validation
const { otherNameRules, otherNameValidation } = require('../other-name/validation.js')
const { otherDOBRules, otherDOBValidation } = require('../other-dob/validation.js')
const { otherNHSNumberRules, otherNHSNumberValidation } = require('../other-nhs-number/validation.js')
const { otherAddressRules, otherAddressValidation } = require('../other-address/validation.js')
const { otherDeclarationRules, otherDeclarationValidation } = require('../declaration-other/validation.js')

// Mother Validation
const { mothersNameRules, mothersNameValidation } = require('../mothers-name/validation.js')
const { mothersDOBRules, mothersDOBValidation } = require('../mothers-dob/validation.js')
const { otherMothersNameRules, otherMothersNameValidation } = require('../other-mothers-name/validation.js')
const { otherMothersDOBRules, otherMothersDOBValidation } = require('../other-mothers-dob/validation.js')

// polio Validation
const { polioNameRules, polioNameValidation } = require('../polio-name/validation.js')
const { polioDOBRules, polioDOBValidation } = require('../polio-dob/validation.js')
const { polioDatesRules, polioDatesValidation } = require('../polio-dates/validation.js')
const { polioNotesRules, polioNotesValidation } = require('../polio-notes/validation.js')




const router = new express.Router()

const markdownFolder = 'app'

// Read directory and loops files
// eslint-disable-next-line handle-callback-err
fs.readdir(markdownFolder, async (err, files) => {
  const routes = files.filter(f => f !== 'router.js')
  for (const file of routes) {
    // Remove extension from filename
    const indexPath = file
    // Use filename as route unless index file then replace with root
    const paths = {
      index: (indexPath === 'index') ? '/' : `/${indexPath}`
    }
    // get the default controller and render the content
    const control = await getController(indexPath).render
    router.get(paths.index, control)
  }
})

// Add custom post routes
// Auth Routes
router.post('/existing-claim', existingClaimRules(), existingClaimValidation(), ExistingClaim().render)
router.post('/user-auth', userAuthRules(), userAuthValidation(), UserAuth().render)
router.post('/user-verify', userVerifyRules(), userVerifyValidation(), UserVerify().render)
router.post('/verification-code', verificationRules(), verificationValidation(), VerificationCode().render)
router.post('/verification-dob', verificationDOBRules(), verificationDOBValidation(), VerificationDOB().render)
router.post('/save-claim', SaveClaimRules(), SaveClaimValidation(), SaveClaim().render)
router.get('/store-claim', StoreClaimRules(), StoreClaimValidation(), StoreClaim().render)

// Intro Routes
router.post('/who-are-you-applying-for', whoAreYouApplyingForRules(), whoAreYouApplyingForValidation(), WhoAreYouApplyingFor().render)
router.post('/med-records-consent', medRecordsConsentRules(), medRecordsConsentValidation(), MedRecordsConsent().render)
router.post('/previous-claim-question', previousClaimQuestionRules(), previousClaimQuestionValidation(), PreviousClaimQuestion().render)
router.post('/other-previous-claim-question', otherPreviousClaimQuestionRules(), otherPreviousClaimQuestionValidation(), OtherPreviousClaimQuestion().render)
router.post('/why-are-you-making-this-claim', whyAreYouMakingThisClaimRules(), whyAreYouMakingThisClaimValidation(), WhyAreYouMakingThisClaim().render)
router.post('/other-why-are-you-making-this-claim', otherWhyAreYouMakingThisClaimRules(), otherWhyAreYouMakingThisClaimValidation(), OtherWhyAreYouMakingThisClaim().render)
router.post('/died-med-records-consent', diedMedRecordsConsentRules(), diedMedRecordsConsentValidation(), DiedMedRecordsConsent().render)
router.post('/other-med-records-consent', otherMedRecordsConsentRules(), otherMedRecordsConsentValidation(), OtherMedRecordsConsent().render)

// About Routes
router.post('/about-you-name', aboutYouNameRules(), aboutYouNameValidation(), AboutYouName().render)
router.post('/about-you-dob', aboutYouDOBRules(), aboutYouDOBValidation(), AboutYouDOB().render)
router.post('/about-you-nhs-number', aboutYouNHSNumberRules(), aboutYouNHSNumberValidation(), AboutYouNHSNumber().render)
router.post('/about-you-address', aboutYouAddressRules(), aboutYouAddressValidation(), AboutYouAddress().render)
router.post('/about-you-contact-details', aboutYouContactDetailsRules(), aboutYouContactDetailsValidation(), AboutYouContactDetails().render)
router.post('/about-you-contact-prefs', aboutYouContactPrefsRules(), aboutYouContactPrefsValidation(), AboutYouContactPrefs().render)
router.get('/about-you-cya', AboutYouCYA().render)
router.get('/other-about-you-cya', OtherAboutYouCYA().render)
router.get('/user-auth-resend', UserAuthResend().render)
router.post('/about-you-died-relationship', aboutYouDiedRelationshipRules(), aboutYouDiedRelationshipValidation(), AboutYouDiedRelationship().render)
router.post('/about-you-under16-relationship', aboutYouUnder16RelationshipRules(), aboutYouUnder16RelationshipValidation(), AboutYouUnder16Relationship().render)

// Your Application
router.get('/your-application', YourApplication().render)
router.get('/accountNotVerified', AccountNotVerifiedCYA().render)

// Representative
router.post('/representative-question', representativeQuestionRules(), representativeQuestionValidation(), RepresentativeQuestion().render)
router.post('/representative-name', representativeNameRules(), representativeNameValidation(), RepresentativeName().render)
router.post('/representative-pos', representativePosRules(), representativePosValidation(), RepresentativePos().render)
router.post('/representative-address', representativeAddressRules(), representativeAddressValidation(), RepresentativeAddress().render)
router.post('/representative-contact', representativeContactRules(), representativeContactValidation(), RepresentativeContact().render)
router.get('/representative-cya', RepresentativeCYA().render)

// Vaccine
router.post('/add-vaccine', addVaccineQuestionRules(), addVaccineQuestionValidation(), AddVaccine().render)
router.post('/add-vaccine-died', addVaccineDiedQuestionRules(), addVaccineDiedQuestionValidation(), AddVaccineDied().render)
router.post('/add-vaccine-other', addVaccineOtherQuestionRules(), addVaccineOtherQuestionValidation(), AddVaccineOther().render)
router.post('/add-vaccine-location', addVaccineLocationQuestionRules(), addVaccineLocationValidation(), AddVaccineLocation().render)
router.post('/armed-forces-check', armedForcesCheckRules(), armedForcesCheckValidation(), ArmedForcesCheck().render)
router.post('/armed-forces-check-died', armedForcesCheckDiedRules(), armedForcesCheckDiedValidation(), ArmedForcesCheckDied().render)
router.post('/add-vaccine-details', addVaccineDetailsRules(), addVaccineDetailsValidation(), AddVaccineDetails().render)
router.post('/add-vaccine-summary', addVaccineSummaryRules(), addVaccineSummaryValidation(), AddVaccineSummary().render)
router.get('/delete-vaccine', DeleteVaccine().render)
router.post('/vaccine-notes', vaccineNotesRules(), vaccineNotesValidation(), VaccineNotes().render)
router.post('/add-covid19-type', addCovid19TypeValidation(), addCovid19Type.post)
router.post('/add-vaccine-country', addVaccineCountryValidation(), addVaccineCountry.post)
router.post('/vaccine-diagnosis', vaccineDiagnosisValidation(), vaccineDiagnosis.post)

// GP Details
router.post('/gp-details', gpDetailsRules(), gpDetailsValidation(), GPDetails().render)
router.post('/gp-details-died', gpDetailsDiedRules(), gpDetailsDiedValidation(), GPDetailsDied().render)
router.post('/gp-details-other', gpDetailsOtherRules(), gpDetailsOtherValidation(), GPDetailsOther().render)

// Hospital
router.post('/visited-hospital', visitedHospitalRules(), visitedHospitalValidation(), VisitHospital().render)
router.post('/visited-hospital-died', visitedHospitalDiedRules(), visitedHospitalDiedValidation(), VisitHospitalDied().render)
router.post('/visited-hospital-other', visitedHospitalOtherRules(), visitedHospitalOtherValidation(), VisitHospitalOther().render)
router.post('/hospital-details', hospitalDetailsRules(), hospitalDetailsValidation(), HospitalDetails().render)
router.get('/hospital-cya', HospitalCYAGet().render)
router.post('/hospital-cya', hospitalCYARules(), hospitalCYAValidation(), HospitalCYAPost().render)

// Healthcare Providers
router.post('/visited-healthcare-provider', visitedHealthcareProviderRules(), visitedHealthcareProviderValidation(), VisitHealthcareProvider().render)
router.post('/healthcare-provider-details', healthcareProviderDetailsRules(), healthcareProviderDetailsValidation(), HealthcareProviderDetails().render)
router.get('/healthcare-provider-cya', HealthcareProviderCYAGet().render)
router.post('/healthcare-provider-cya', healthcareProviderCYARules(), healthcareProviderCYAValidation(), HealthcareProviderCYAPost().render)

// Medical
router.get('/medical-cya', MedicalCYA().render)

// Upload documents
router.post('/upload-documents', uploadDocumentsRules(), uploadDocumentsValidation(), UploadDocuments().render)
router.post('/delete-document', deleteDocumentRules(), deleteDocumentValidation(), DeleteDocument().render)

// Declaration
router.post('/declaration', declarationRules(), declarationValidation(), Declaration().render)

//Died
router.post('/died-name', diedNameRules(), diedNameValidation(), DiedName().render)
router.post('/died-dob', diedDOBRules(), diedDOBValidation(), DiedDOB().render)
router.post('/died-dod', diedDODRules(), diedDODValidation(), DiedDOD().render)
router.post('/died-nhs-number', diedNHSNumberRules(), diedNHSNumberValidation(), DiedNHSNumber().render)
router.post('/died-address', diedAddressRules(), diedAddressValidation(), DiedAddress().render)
router.get('/died-cya', DiedCYA().render)

//Other
router.post('/other-name', otherNameRules(), otherNameValidation(), OtherName().render)
router.post('/other-dob', otherDOBRules(), otherDOBValidation(), OtherDOB().render)
router.post('/other-nhs-number', otherNHSNumberRules(), otherNHSNumberValidation(), OtherNHSNumber().render)
router.post('/other-address', otherAddressRules(), otherAddressValidation(), OtherAddress().render)
router.get('/other-cya', OtherCYA().render)

//Mother
router.post('/mothers-name', mothersNameRules(), mothersNameValidation(), MothersName().render)
router.post('/mothers-dob', mothersDOBRules(), mothersDOBValidation(), MothersDOB().render)
router.get('/mothers-cya', MothersCYA().render)
router.post('/other-mothers-name', otherMothersNameRules(), otherMothersNameValidation(), OtherMothersName().render)
router.post('/other-mothers-dob', otherMothersDOBRules(), otherMothersDOBValidation(), OtherMothersDOB().render)
router.get('/other-mothers-cya', OtherMothersCYA().render)

//Polio
router.post('/polio-name', polioNameRules(), polioNameValidation(), PolioName().render)
router.post('/polio-dob', polioDOBRules(), polioDOBValidation(), PolioDOB().render)
router.get('/polio-cya', PolioCYA().render)
router.post('/polio-dates', polioDatesRules(), polioDatesValidation(), PolioDates().render)
router.post('/polio-notes', polioNotesRules(), polioNotesValidation(), PolioNotes().render)

// Export
module.exports = {
  router
}
