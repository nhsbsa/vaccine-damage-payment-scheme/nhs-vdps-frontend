'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const vpRepository = require('../../common/schemas/VaccinatedPersonDetails')
const dataRepository = require('../../common/schemas/Data')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const { formatDateString } = require('../../common/utils/string');

module.exports = () => {
    return {
        render: async (req, res) => {
            try {
                let dobDay = req.body.dobDay
                let dobMonth = req.body.dobMonth
                const dobYear = req.body.dobYear

                 if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                    req.session.body = req.body
                    res.redirect('mothers-dob')
                    return
                  } else {
                    const repo = (await vpRepository()).repository
                    const vpData = await repo.fetch(req.cookies.sessionID)
                    vpData.VpDateOfBirth = formatDateString(dobYear, dobMonth, dobDay)
                    await repo.save(vpData)
                  }




            } catch (e) {
                logRequestStart({
                    level: 'error',
                    message: e.message
                })
            }
            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('mothers-dob')
                return
            }

                        if(req.session.errors){
if(!req.session.errors.length){
            req.session.errors = null
        }
        }
            const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)
            res.redirect('mothers-cya')
            return
        }
    }
}
