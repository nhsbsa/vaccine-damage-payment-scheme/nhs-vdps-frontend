'use strict'
const dataRepository = require('../../common/schemas/Data')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const querystring = require('node:querystring');

module.exports = () => {
  return {
    render: async (req, res) => {

        try {
          const dataRepo = (await dataRepository()).repository
          const data = await dataRepo.fetch(req.cookies.sessionID)


          const dpRepo = (await dpRepository()).repository
          const dp = await dpRepo.fetch(req.cookies.sessionID)
          dp.DpAddressLine1 = req.body.DpAddressLine1
          dp.DpAddressLine2 = req.body.DpAddressLine2 ? req.body.DpAddressLine2 : null
          dp.DpTownOrCity = req.body.DpTownOrCity
          dp.DpCounty = req.body.DpCounty ? req.body.DpCounty : null
          dp.DpPostcode = req.body.DpPostcode
          await dpRepo.save(dp)
          if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
            res.redirect('other-address')
            return
          }
                        if(req.session.errors){
if(!req.session.errors.length){
            req.session.errors = null
        }
        }
            res.redirect('other-cya')
        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })

        }
    }
  }
}
