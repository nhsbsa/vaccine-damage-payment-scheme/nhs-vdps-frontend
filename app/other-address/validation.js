const { check, validationResult } = require('express-validator')

// this sets the rules on the request body in the middleware
const otherAddressRules = () => {
  return [
    check('DpAddressLine1')
      .trim()
      .isLength({ min: 1, max: 100 })
      .withMessage('common.errors.E0001')
      .bail()
      .matches(/^([a-zA-Z0-9 \-,:'\/])*$/)
      .withMessage('common.errors.E0002'),
    check('DpAddressLine2')
      .trim()
      .optional({ checkFalsy: true, nullable: true })
      .isLength({ max: 100 })
      .withMessage('common.errors.E0003')
      .bail()
      .matches(/^([a-zA-Z0-9 \-,:'\/])*$/)
      .withMessage('common.errors.E0004'),
    check('DpTownOrCity')
      .trim()
      .isLength({ min: 2 })
      .withMessage('common.errors.E0005')
       .bail()
       .isLength({ max: 100 })
       .withMessage('common.errors.E0006')
       .bail()
       .matches(/^[a-zA-Z- ]+$/)
       .withMessage('common.errors.E0007'),
    check('DpCounty')
      .trim()
      .optional({ checkFalsy: true, nullable: true })
      .isLength({ min: 3 })
      .withMessage('common.errors.E0008')
       .bail()
       .isLength({ max: 100 })
       .withMessage('common.errors.E0009')
       .bail()
       .matches(/^[a-zA-Z ]+$/)
       .withMessage('common.errors.E0010'),
    check('DpPostcode')
       .trim()
       .isLength({ min: 5, max: 8 })
       .withMessage('common.errors.E0011')
       .bail()
       .matches(/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})$/)
       .withMessage('common.errors.E0011')
  ]
}

// while this function executes the actual rules against the request body
const otherAddressValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)

if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }


    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
            req.session.errors = null
        }
        }
      // heres where you send the errors or do whatever you please with the error, in  your case

    }

    next()
  }
}
module.exports = {
  otherAddressValidation,
  otherAddressRules
}
