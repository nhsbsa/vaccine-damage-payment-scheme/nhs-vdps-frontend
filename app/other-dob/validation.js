const { check, validationResult } = require('express-validator')
const dpRepository = require('../../common/schemas/DisabledPersonDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const querystring = require('node:querystring');
const validateDateString = require("../../common/utils/validate-date-string")
const { formatDateString } = require('../../common/utils/string');

// this sets the rules on the request body in the middleware
const otherDOBRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const otherDOBValidation = () => {
  return async (req, res, next) => {
    
    if(req.session.errors){
        if(req.session.errors.length){
                req.session.errors = null
        }
    }
    // execute the rules
    const errors = validationResult(req)
    const errorsInput = []

    const dobDay = req.body.dobDay
    const dobMonth = req.body.dobMonth
    const dobYear = req.body.dobYear

    if (dobDay === '' && dobMonth === '' && dobYear === '') {
          errors.errors.push({
            msg: 'common.errors.E0017',
            param: 'dobDay',
            formgroup: 'dobDate'
          })
        }

        if (errors.errors.length === 0) {
          if (dobDay === '' || (dobDay < 1 && dobDay > 31)) {
            errors.errors.push({
              msg: 'common.errors.E0018',
              param: 'dobDay',
              formgroup: 'dobDate'
            })
          }
          if (dobMonth === '' || (dobMonth < 1 && dobMonth > 12)) {
            errors.errors.push({
              msg: 'common.errors.E0019',
              param: 'dobMonth',
              formgroup: 'dobDate'
            })
          }
          if (dobYear === '') {
            errors.errors.push({
              msg: 'common.errors.E0020',
              param: 'dobYear',
              formgroup: 'dobDate'
            })

          }
          if (dobYear !== '' && dobYear.length != 4) {
            errors.errors.push({
              msg: 'common.errors.E0021',
              param: 'dobYear',
              formgroup: 'dobDate'
            })
          }
        }

        if (errors.errors.length === 0) {
          if (!validateDateString(dobDay, dobMonth, dobYear)) {
            errors.errors.push({
              msg: 'common.errors.E0022',
              param: 'dobDay',
              formgroup: 'dobDate'
            })
          }
        }


    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            }
        }
    }
    // move to next middleware
    next()
  }
}



function isDateInTheFuture(day, month, year){
    const date = new Date(year + '-' + month + '-' + day)
    const today = new Date()

    return date > today
}

module.exports = {
  otherDOBValidation,
  otherDOBRules
}
