'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const vpRepository = require('../../common/schemas/VaccinatedPersonDetails')
const dataRepository = require('../../common/schemas/Data')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
;
module.exports = () => {
  return {
    render: async (req, res) => {
        try {
              const repo = (await vpRepository()).repository
              const vp = await repo.fetch(req.cookies.sessionID)
              vp.VpTitle = req.body.VpTitle
              vp.VpSurname = req.body.VpSurname
              vp.VpMiddlename = req.body.VpMiddlename ? req.body.VpMiddlename : null
              vp.VpForename = req.body.VpForename
              await repo.save(vp)
              if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                  res.redirect('other-mothers-name')
                  return
              }

              const dataRepo = (await dataRepository()).repository
              const data = (await dataRepo.fetch(req.cookies.sessionID)).toJSON()

              res.redirect('other-mothers-dob')

              return
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.message
              })
            }
          res.redirect('other-mothers-name')
          return

    }
  }
}
