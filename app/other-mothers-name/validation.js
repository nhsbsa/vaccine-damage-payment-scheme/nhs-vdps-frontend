const { check, validationResult } = require('express-validator')
const { logRequestStart } = require('../../common/utils/request-logger')

// this sets the rules on the request body in the middleware
const otherMothersNameRules = () => {
  return [
    check('VpForename')
        .trim()
        .isLength({ min: 2 })
        .withMessage('common.errors.E0024')
        .bail()
        .isLength({ max: 100 })
        .withMessage('common.errors.E0025')
        .bail()
        .matches(/^[A-Za-z \'\-]+$/)
        .withMessage('common.errors.E0026'),
    check('VpMiddlename')
        .trim()
        .optional({ checkFalsy: true, nullable: true })
        .isLength({ min: 2 })
        .withMessage('common.errors.E0027')
        .isLength({ max: 100 })
        .withMessage('common.errors.E0028')
        .bail()
        .matches(/^[A-Za-z \'\-]+$/)
        .withMessage('common.errors.E0029'),
    check('VpSurname')
        .trim()
        .isLength({ min: 2 })
        .withMessage('common.errors.E0030')
        .bail()
        .isLength({ max: 100 })
        .withMessage('common.errors.E0031')
        .bail()
        .matches(/^[A-Za-z \'\-]+$/)
        .withMessage('common.errors.E0032')
  ]
}

// while this function executes the actual rules against the request body
const otherMothersNameValidation = () => {
  return async (req, res, next) => {
    // execute the rules
    const errors = validationResult(req)
if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
            req.session.errors = null
        }
        }
    }
    // move to next middleware
    next()
  }
}
module.exports = {
  otherMothersNameValidation,
  otherMothersNameRules
}
