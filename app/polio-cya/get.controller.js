'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const metaRepository = require('../../common/schemas/MetaData')
const dataRepository = require('../../common/schemas/Data')
const vpRepository = require('../../common/schemas/VaccinatedPersonDetails')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const claimRepository = require('../../common/schemas/Claim')
const htmlentities = require('../../common/assets/htmlentities.json')
module.exports = () => {
  return {
    render: async (req, res) => {
      try {
            req.session.section = 'VaccinesReceived'
        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)

        if(!data.AccountVerified || !data.WhyMakingClaim ){
            res.redirect('/')
            return
        }

        const metaRepo = (await metaRepository()).repository
        const metadata = await metaRepo.fetch(req.cookies.sessionID)

        const vpRepo = (await vpRepository()).repository
        const vp = await vpRepo.fetch(req.cookies.sessionID)

        const claimRepo = (await claimRepository()).repository
        const claim = await claimRepo.fetch(req.cookies.sessionID)


        const applicationStatusRepo = (await applicationStatusRepository()).repository
        const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
        const applicationStatusU = (await applicationStatusRepo.fetch(req.cookies.sessionID)).toJSON()

        const checkSection = function(applicationStatus, req) {

            const route = req.route.path.replace('/','')

            const sectionsToCheck = ['YourDetails','NominatedPersonDetails','VaccinesReceived','DatesOfVaccination','WhatHappenedNext','DoctorDetails','HospitalAttendance','HealthClinicAttendance','Declaration']
            const section = req.session.section

            sectionsToCheck.length = section ? sectionsToCheck.indexOf(section)+1 : 0
            for (const check of sectionsToCheck){
                if(req.session.errors){
                    if(req.session.errors.length){
                        if(req.session.errors.find(e => e.section === check)){
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        if(!checkSection(applicationStatus, req)){
            applicationStatus.VaccinatedPersonDetails = 'Completed'
            if(applicationStatusU.DatesOfVaccination !== 'Completed'){
                applicationStatus.DatesOfVaccination = 'ToDo'
            }
            applicationStatus.VaccinesReceived = 'Completed'
        } else {
           applicationStatus.VaccinatedPersonDetails = 'ToDo'
           applicationStatus.DatesOfVaccination = 'ToDo'
            applicationStatus.VaccinesReceived = 'ToDo'
        }
        await applicationStatusRepo.save(applicationStatus)


        const templateBody = findFile('polio-cya')
        let pageTitle;
        let md = templateBody.toString();
        if(md.includes('{#<pagetitle>')){
            pageTitle = md.split('{#<pagetitle>')[1].split('</pagetitle>')[0]
            md = md.split('</pagetitle>#}')[1]
        }
        let errors = req.session.errors ? req.session.errors.filter(e => e.route === req.route.path) : null
        if(errors){
            if(!errors.length)
                errors = null
        }

    
        const markdownRender = markdown(templateBody.toString())
        const params = {
          content: markdownRender,
          file: `./app/polio-cya/templates/index.njk`,
          errors: errors,
          data: {
            VaccinatedPersonDetails: vp,
            MetaData: metadata,
            Data: data,
            Claim: claim
          },
          title: pageTitle ? pageTitle: 'PLEASE SET A PAGE TITLE'
        }
        res.render('app/index/index', params)
      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })

        return res.redirect('back')
      }
    }
  }
}
