const { check, validationResult } = require('express-validator')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const querystring = require('node:querystring');
const validateDateString = require("../../common/utils/validate-date-string")

// this sets the rules on the request body in the middleware
const polioDatesRules = () => {
  return [

  ]
}

// while this function executes the actual rules against the request body
const polioDatesValidation = () => {
  return async (req, res, next) => {
    req.session.errors = null
    // execute the rules
    const errors = validationResult(req)
    const errorsInput = []

    const vaccineDay = req.body.vaccineDay
    const vaccineMonth = req.body.vaccineMonth
    const vaccineYear = req.body.vaccineYear

    const PolioDatesUnknown = req.body.PolioDatesUnknown
    const vaccineType = req.body.vaccineType

    // 'I don't know option always take priority if user has chosen
    if (vaccineDay === '' && vaccineMonth === '' && vaccineYear === '' && PolioDatesUnknown !== "Unknown") {
        errors.errors.push({
            msg: 'common.errors.E0016',
            param: 'vaccineDay',
            formgroup: 'vaccineDate'
        })
    }
    else{
        if ((!PolioDatesUnknown) || PolioDatesUnknown === "undefined" ){
            if(vaccineDay === '' && vaccineMonth === '' && vaccineYear === '' && PolioDatesUnknown !== "Unknown") {
              errors.errors.push({
                msg: 'common.errors.E0099',
                param: 'vaccineDay',
                formgroup: 'vaccineDate'
              })
            }
            if (errors.errors.length === 0) {
              if (vaccineDay === ''|| (vaccineDay < 1 && vaccineDay > 31)) {
                errors.errors.push({
                  msg: 'common.errors.E0100',
                  param: 'vaccineDay',
                  formgroup: 'vaccineDate'
                })
              }
              if (vaccineMonth === ''|| (vaccineMonth < 1 && vaccineMonth > 12)) {
                errors.errors.push({
                  msg: 'common.errors.E0101',
                  param: 'vaccineMonth',
                  formgroup: 'vaccineDate'
                })
              }
              if (vaccineYear === '') {
                errors.errors.push({
                  msg: 'common.errors.E0102',
                  param: 'vaccineYear',
                  formgroup: 'vaccineDate'
                })
              }
              if (vaccineYear !== '' && vaccineYear.length != 4) {
                errors.errors.push({
                  msg: 'common.errors.E0103',
                  param: 'vaccineYear',
                  formgroup: 'vaccineDate'
                })
              }
            }
            if (errors.errors.length === 0) {
              if (!validateDateString(vaccineDay, vaccineMonth, vaccineYear)) {
                errors.errors.push({
                  msg: 'common.errors.E0104',
                  param: 'vaccineDay',
                  formgroup: 'vaccineDate'
                })
              }
            }

            if (errors.errors.length === 0) {
                if (!isDateInPast(vaccineDay, vaccineMonth, vaccineYear)) {
                    errors.errors.push({
                        msg: 'common.errors.E0105',
                        param: 'vaccineDay',
                        formgroup: 'vaccineDate'
                    })
                }
            }
        }
    }

    if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
    // check if any of the request body failed the requirements set in validation rules
    if (errors.isEmpty()) {
        if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
                req.session.errors = null
            }
        }
    }
    // move to next middleware
    next()
  }
}

function isDateInPast (day, month, year) {
  const today = new Date()
  const vaccineDate = new Date(year + '-' + month + '-' + day)
  return today>vaccineDate
}
module.exports = {
  polioDatesRules,
  polioDatesValidation
}
