'use strict'
const vnRepository = require('../../common/schemas/VaccineNotes')
const applicationStatusRepository = require('../../common/schemas/ApplicationStatus')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const querystring = require('node:querystring');

module.exports = () => {
  return {
    render: async (req, res) => {
    req.session.section = 'VaccinesReceived'
    const repo = (await vnRepository()).repository
    const data = await repo.fetch(req.cookies.sessionID)

      data.Description = req.body.description
      await repo.save(data)

      const applicationStatusRepo = (await applicationStatusRepository()).repository
      const applicationStatus = await applicationStatusRepo.fetch(req.cookies.sessionID)
      applicationStatus.WhatHappenedNext = 'Completed'
      if(applicationStatus.DoctorDetails !== 'Completed'){
        applicationStatus.DoctorDetails = 'ToDo'
      }

      await applicationStatusRepo.save(applicationStatus)

       if(req.session.errors){
          res.redirect('polio-notes')
          return
        }
      res.redirect('vaccine-cya')
    }
  }
}
