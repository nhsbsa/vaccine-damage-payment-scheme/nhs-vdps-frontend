const { check, validationResult } = require('express-validator')
const vnRepository = require('../../common/schemas/VaccineNotes')
var sanitizer = require('sanitizer')
// this sets the rules on the request body in the middleware
const polioNotesRules = () => {
    return [
        check('description')
        .isLength({ min: 1 })
        .withMessage('common.errors.E0098')
        .bail()
        .isLength({ max: 2000 })
        .withMessage('common.errors.E0121')
        .bail()
    ]
}

// while this function executes the actual rules against the request body
const polioNotesValidation = () => {
    return async (req, res, next) => {
        const errors = validationResult(req)

if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
        const repo = (await vnRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)
        data.Description = req.body.description
        await repo.save(data)

        req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path}))
        // check if any of the request body failed the requirements set in validation rules
        if (!errors.isEmpty()) {
            // heres where you send the errors or do whatever you please with the error, in  your case
            res.redirect('polio-notes')
            return
        }
        // if everything went well, and all rules were passed, then move on to the next middleware
        req.session.errors = null
        next()

    }
}
module.exports = {
  polioNotesRules,
  polioNotesValidation
}
