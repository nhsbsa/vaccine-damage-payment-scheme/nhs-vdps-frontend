const { check, validationResult } = require('express-validator')

// this sets the rules on the request body in the middleware
const representativeContactRules = () => {
    return [
        check('RepresentativeMobileTelNo')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)
            .withMessage('common.errors.E0087')
            .bail()
            .isLength({ min: 11 })
            .withMessage('common.errors.E0088')
            .isLength({ max: 25 })
            .withMessage('common.errors.E0089'),
        check('RepresentativeHomeTelNo')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)
            .withMessage('common.errors.E0090')
            .bail()
            .isLength({ max: 25 })
            .withMessage('common.errors.E0091')
            .bail()
            .isLength({ min: 11 })
            .withMessage('common.errors.E0092'),
        check('RepresentativeWorkTelNo')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .matches(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)
            .withMessage('common.errors.E0093')
            .bail()
            .isLength({ max: 25 })
            .withMessage('common.errors.E0094')
            .bail()
            .isLength({ min: 11 })
            .withMessage('common.errors.E0095'),
        check('RepresentativeEmailAddress')
            .trim()
            .optional({ checkFalsy: true, nullable: true })
            .isLength({ min: 7 })
            .withMessage('common.errors.E0046')
            .isLength({ max: 255 })
            .withMessage('common.errors.E0047')
            .bail()
            .isEmail()
            .withMessage('common.errors.E0048')
    ]
}

// while this function executes the actual rules against the request body
const representativeContactValidation = () => {
  return async (req, res, next) => {
      // execute the rules
      const errors = validationResult(req)
if(req.session.errors){
        req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
            req.session.errors = [...req.session.errors, ...errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))]
        }else{
          req.session.errors = errors.errors.map(err => Object.assign({},err,{route: req.route.path, section: req.session.section}))
    }
      // check if any of the request body failed the requirements set in validation rules
      if (errors.isEmpty()) {
if(req.session.errors){
            req.session.errors = req.session.errors.filter(e => e.route !== req.route.path)
        }
        if(!req.session.errors){
            if(!req.session.errors.length){
            req.session.errors = null
        }
        }
      }
      // move to next middleware
      next()
    }
}
module.exports = {
  representativeContactRules,
  representativeContactValidation
}
