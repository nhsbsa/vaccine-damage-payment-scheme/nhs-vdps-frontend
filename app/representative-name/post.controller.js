'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const representativeRepository = require('../../common/schemas/Representative')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
;
module.exports = () => {
  return {
    render: async (req, res) => {
          req.session.section = 'NominatedPersonDetails'

        try {
              const repo = (await representativeRepository()).repository
              const data = await repo.fetch(req.cookies.sessionID)
              data.RepresentativeSurname = req.body.RepresentativeSurname
              data.RepresentativeMiddlename = req.body.RepresentativeMiddlename ? req.body.RepresentativeMiddlename : null
              data.RepresentativeForename = req.body.RepresentativeForename
              await repo.save(data)
               if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                  res.redirect('representative-name')
                  return
                }
              res.redirect('representative-pos')
                return
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.message
              })
            }
          res.redirect('representative-name')
          return

    }
  }
}
