'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const representativeRepository = require('../../common/schemas/Representative')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
;
module.exports = () => {

  return {
    render: async (req, res) => {
          req.session.section = 'NominatedPersonDetails'

        try {
            const repRepo = (await representativeRepository()).repository
            const representative = await repRepo.fetch(req.cookies.sessionID)
            representative.RepresentativePlaceOfStay = req.body.RepresentativePlaceOfStay
            await repRepo.save(representative)

            if(req.body.RepresentativePlaceOfStay === 'Yes'){
                const cdRepo = (await cdRepository()).repository
                const cd = await cdRepo.fetch(req.cookies.sessionID)
                representative.RepresentativeAddressLine1  = cd.CdAddressLine1
                representative.RepresentativeAddressLine2 = cd.CdAddressLine2
                representative.RepresentativeTownOrCity = cd.CdTownOrCity
                representative.RepresentativeCounty = cd.CdCounty
                representative.RepresentativePostcode = cd.CdPostcode
                await repRepo.save(representative)
                 if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                    res.redirect('representative-pos')
                    return
                  }
                res.redirect('representative-contact')
            } else {
                res.redirect('representative-address')
            }
            return
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }
      res.redirect('representative-pos')
      return

    }
  }
}
