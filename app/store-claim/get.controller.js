'use strict'
const dataRepository = require('../../common/schemas/Data')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const { storeClaim, data, updateClaim } = require('../../common/utils/claim')

module.exports = () => {
  return {
    render: async (req, res) => {
        try{
            const claimData = await data(req, true)
            const store = await storeClaim(claimData)
            if(store.data.success || store.success){
                res.redirect('done');
                return
            }
        }catch(e){
            if(e.response){
                if(e.response.data){
                    if(!e.response.data.success) {
                    /* logRequestStart({
                           level: 'info',
                           message: 'save-claim / POST / storeClaim - ' + e.response.data.messages
                       })*/
                         if(e.response.data.messages === 'A claim already exists') {
                            try{
                                const claimData = await data(req, true)
                                const update = await updateClaim(claimData)
                                if(update.data.success){
                                    res.redirect('done');
                                    return

                                }
                            }catch(e){
                             logRequestStart({
                                   level: 'error',
                                   message: 'save-claim / POST / updateClaim - ' + e.response.data.messages
                               })
                            }
                        }else{
                            req.session.errors = e.response.data.errors.map(error => ({
                                param: 'body',
                               msg: error.name + ' ' + error.messages.join(', '),
                               route: req.route.path,
                               location: 'body'
                            }))
                            res.redirect('error-page');
                            return
                        }
                    }
                }
            } else {
                res.redirect('error-page');
               return
            }
        }

    }
  }
}
