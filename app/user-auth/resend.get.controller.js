'use strict'
const {notifyClient, sendVerificationCode} = require('../../common/utils/claim')
const { logRequestStart } = require('../../common/utils/request-logger')
const metaRepository = require('../../common/schemas/MetaData')

module.exports = () => {
  return {
    render: async (req, res) => {
        const metaRepo = (await metaRepository()).repository
        const meta = await metaRepo.fetch(req.cookies.sessionID)
        if(!meta.Email){
            res.redirect('user-auth')
            return
        }

        var time = (Date.now() / 1000) - (5 * 60); // Five minutes ago
        if((req.session.blocked - time) > 0) {
            logRequestStart({
                level: 'error',
                message: `User blocked for another ${req.session.blocked - time} seconds`
              })
        }else{
            req.session.blocked = null
            if(req.session.errors){
                if(!req.session.errors.length){
                    req.session.errors = null
                }
            }
        }

        if(meta.ResendCodeCount >= 5){
            req.session.blocked =  (Date.now() / 1000)
            meta.ResendCodeCount = 0
            metaRepo.save(meta)
        }
        if(req.session.blocked){
            req.session.errors = [{
                route: '/user-verify',
                msg: `common.errors.E0116`,
                param: 'code'
            }]
            res.redirect('user-verify')
            return
        }
        try {
          const verification = await sendVerificationCode(meta.Email)
          if (verification.status === 201) {
            try {
              const expirationTime = Date.now() + 30 * 60000
              const date = new Date(expirationTime)
              const expiryDate = date.toString().substring(0, 15)
              const expiryTime = date.toString().substring(16, 24)

              const notify = await notifyClient(meta.Email, verification.data.items.code, expiryDate, expiryTime, verification.data.items.tokval, 'existing')
              if (notify.status === 201) {

                meta.ResendCodeCount = meta.ResendCodeCount+1
                metaRepo.save(meta)
                if(req.session.errors){
                    if(!req.session.errors.length){
                        req.session.errors = null
                    }
                }
                if(req.query.returnPage === 'user-verify'){
                    res.redirect('user-verify')
                } else {
                    res.redirect('verification-code')
                }
                return
              } else {
                logRequestStart({
                  level: 'error',
                  message: "Error sending calling notifyClient"
                })
              }
            } catch (e) {
              logRequestStart({
                level: 'error',
                message: e.message
              })
            }
          }
        } catch (e) {
          logRequestStart({
            level: 'error',
            message: e.message
          })
          req.session.errors = [
            {
              param: 'email',
              msg: e.message,
              value: meta.Email,
              location: 'body'
            }
          ]
          res.redirect('user-auth')
          return
        }
        res.redirect('user-auth')
        return
    }
  }
}
