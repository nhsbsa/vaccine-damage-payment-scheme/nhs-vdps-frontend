const { logRequestStart } = require('../../common/utils/request-logger');
const vnRepository = require('../../common/schemas/VaccineNotes')

module.exports.post = async (req, res) => {
    try {
        const diagnosis = Array.isArray(req.body.diagnosis) ? req.body.diagnosis : [req.body.diagnosis];
        const { repository: vnRepo } = await vnRepository();

        const vnData = await vnRepo.fetch(req.cookies.sessionID);
        vnData.Diagnosis = diagnosis;
        await vnRepo.save(vnData);

        return res.redirect('vaccine-notes');
    } catch (error) {
        logRequestStart({
            level: 'error',
            message: error.message
        });
        return res.redirect('vaccine-diagnosis');
    }
};
