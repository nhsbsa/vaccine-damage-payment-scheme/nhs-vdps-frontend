const { req, res } = require('express');
const postController = require('./post.controller');
const vnRepository = require('../../common/schemas/VaccineNotes');

jest.mock('../../common/schemas/VaccineNotes');


describe('Post Controller', () => {
    test('should redirect to vaccine-notes page', async () => {
        await postController.post(req, res);
        expect(res.redirect).toHaveBeenCalledTimes(1);
        expect(res.redirect.mock.calls[0][0]).toEqual('vaccine-notes');
    });

    test('should redirect to vaccine-diagnosis page when req is invalid', async () => {
        await postController.post({}, res);
        expect(res.redirect).toHaveBeenCalledTimes(1);
        expect(res.redirect.mock.calls[0][0]).toEqual('vaccine-diagnosis');
    });

    test('should call fetch vnRepo with the correct param', async () => {
        const { repository: vnRepo } = await vnRepository();

        await postController.post(req, res);
        expect(vnRepo.fetch).toHaveBeenCalledTimes(1);
        expect(vnRepo.fetch.mock.calls[0][0]).toEqual('1234');
    });

    test('should call vnRepo.save when diagnosis = "None of the above" ', async () => {
        const { repository: vnRepo } = await vnRepository();
        req.body.diagnosis = 'None of the above';
        await postController.post(req, res);
        expect(vnRepo.save).toHaveBeenCalledTimes(1);
        expect(vnRepo.save.mock.calls[0][0]).toEqual({
            Diagnosis: ['None of the above']
        });
    });

    test('should call vnRepo.save when diagnosis is an array', async () => {
        const { repository: vnRepo } = await vnRepository();
        req.body.diagnosis = ['Myocarditis', 'Pericarditis', 'Narcolepsy'];
        await postController.post(req, res);
        expect(vnRepo.save).toHaveBeenCalledTimes(1);
        expect(vnRepo.save.mock.calls[0][0]).toEqual({
            Diagnosis: ['Myocarditis', 'Pericarditis', 'Narcolepsy']
        });
    });
});
