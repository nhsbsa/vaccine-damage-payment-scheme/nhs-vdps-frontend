'use strict'
const dataRepository = require('../../common/schemas/Data')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const metaRepository = require('../../common/schemas/MetaData')
const { sendVerificationCode, notifyClient, verifyClaim, getClaimData } = require('../../common/utils/claim')
const querystring = require('node:querystring');
;

module.exports = () => {
  return {
    render: async (req, res) => {
        const metaRepo = (await metaRepository()).repository
        const meta = (await metaRepo.fetch(req.cookies.sessionID)).toJSON()
        const email = meta.Email;
        const code = req.body.code;
        try{
            if (!Number(code)){
                req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/verification-code'}]
                return res.redirect('verification-code')
            }
            const verify = await verifyClaim({email: email, code: code});
            if(!verify.data.success){
                logRequestStart({
                    level: 'error',
                    message: verify.data.message
                })
                req.session.errors = [{param: 'code', msg: `common.errors.E0119`, route: '/verification-code'}]
                return res.redirect('verification-code')
            }else{
                try{
                     const claim = await getClaimData(email)
                     if(req.session.errors){
                        if(!req.session.errors.length){
                            req.session.errors = null
                        };
                    }
                    return res.redirect('verification-dob');
                }
                catch(e){
                    if(e.response){
                        if(e.response.data.success && e.response.data.messages === 'No claims found'){
                            req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/verification-code'}]
                            return res.redirect('verification-code')
                        }
                        if(e.response.data.errors && e.response.data.errors.length){
                        }
                        else {
                            if(!e.response.data.success && e.response.data.messages === 'No claims found'){
                                req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/verification-code'}]
                                return res.redirect('verification-code')
                            }
                            logRequestStart({
                               level: 'error',
                               message: `verification-code / POST / getClaimData - ${e}`
                           })
                        }
                    }
                }
            }
        }catch(e) {
            const data = e.data ? e.data : e.response.data
            if(data.success && data.messages === 'No claims found'){
                req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/verification-code'}]
            }
            if(data.errors && (typeof data.errors === 'object')){
                let errors = [];
                data.errors.map(error => {
                    errors.push({
                       param: 'code',
                       msg: error.messages.join(', '),
                       route: req.route.path,
                       value: req.body.code,
                       location: 'body'
                    });

                    logRequestStart({
                       level: 'error',
                       message: 'verification-code / POST / verifyClaim - Error(s): ' + error.name + ': '+ error.messages.join(', ')
                   })
                })
                req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/verification-code'}]
            }
            if(data.errors && (typeof data.errors === 'string')){
                req.session.errors = [{param: 'code', msg: `common.errors.E0118`, route: '/verification-code'}]
                logRequestStart({
                   level: 'error',
                   message: `verification-code / POST / verifyClaim - Error(s): ${data.errors}`
               })
            }
            return res.redirect('verification-code')
        }
    }
  }
}
