'use strict'
const dataRepository = require('../../common/schemas/Data')
const cdRepository = require('../../common/schemas/ClaimantDetails')
const { logRequestStart } = require('../../common/utils/request-logger')
const config = require('../../common/config')
const { findFile, markdown } = require('../../common/utils/markdown')
const metaRepository = require('../../common/schemas/MetaData')
const { sendVerificationCode, notifyClient, verifyClaim, getClaimData, restoreClaim } = require('../../common/utils/claim')
const i18n = require('i18n')
const querystring = require('node:querystring');

module.exports = () => {
  return {
    render: async (req, res) => {
        const metaRepo = (await metaRepository()).repository
        const meta = (await metaRepo.fetch(req.cookies.sessionID)).toJSON()
        const dataRepo = (await dataRepository()).repository
        const data = await dataRepo.fetch(req.cookies.sessionID)
        const email = meta.Email;
        const code = req.body.code;

        let errors = [];
        let errorsInput = [];

        let dobDay = req.body.dobDay;
        let dobMonth = req.body.dobMonth;
        let dobYear = req.body.dobYear;

        let cookie = req.cookies.user_language_cookie;
        if (!cookie) { i18n.setLocale('en') } else { i18n.setLocale(cookie) }
        try{
            if(!req.session.errors || !req.session.errors.length){
                const claim = await getClaimData(email)
                const claimDOB = claim.data.items.Data.Claim.ClaimantDetails.CdDateOfBirth.split('-');
                if((errors.length === 0) && (parseInt(claimDOB[0]) !== parseInt(dobYear) || parseInt(claimDOB[1]) !== parseInt(dobMonth) || parseInt(claimDOB[2]) !== parseInt(dobDay)) ){
                    logRequestStart({
                        level: 'error',
                        message: `common.errors.E0109`
                    })
                    errors = [`common.errors.E0109`]
                }
                if(!errors.length){
                    try{
                        await restoreClaim(claim.data.items, req)
                    }catch(e){
                        logRequestStart({
                            level: 'error',
                            message: e.message
                        })
                    }
                }
            }
        }catch(e) {
            logRequestStart({
                level: 'error',
                message: `common.errors.E0109`
            })
            if(errors.length === 0) {
                errors = [`common.errors.E0109`]
            }
        }

        if (errors.length || req.session.errors) {
            if (errors.length)
                req.session.errors = errors.map(err => ({route: req.route.path, msg: err, param: 'dobDay'}));
                req.session.body = req.body;

            res.redirect('verification-dob')
            return
        } else {
            return res.redirect('your-application');
        }

        if(!req.session.errors || !req.session.errors.length){
            req.session.errors = null
        }
        res.redirect('/your-application')

    }
  }
}