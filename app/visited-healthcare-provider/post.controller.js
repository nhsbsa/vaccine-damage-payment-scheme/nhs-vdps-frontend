'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const dataRepository = require('../../common/schemas/Data')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')

module.exports = () => {

  return {
    render: async (req, res) => {
               req.session.section = 'HealthClinicAttendance'

        try {
            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('visited-healthcare-provider')
                return
            }
             const dataRepo = (await dataRepository()).repository
            const data = await dataRepo.fetch(req.cookies.sessionID)
            data.VisitedHealthProvider = req.body.VisitedHealthCareProvider
            await dataRepo.save(data)

            if(req.body.VisitedHealthCareProvider === 'Yes'){
                res.redirect('healthcare-provider-details')
            } else {
                res.redirect('healthcare-provider-cya')
            }
            return
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }
        if(data.ApplyingFor === 'Deceased'){
            res.redirect('visited-healthcare-provider-died')
        } else
        if(data.ApplyingFor === 'Under16' || data.ApplyingFor === 'Disabled'){
            res.redirect('visited-healthcare-provider-other')
        } else {
            res.redirect('visited-healthcare-provider')
        }

      return

    }
  }
}
