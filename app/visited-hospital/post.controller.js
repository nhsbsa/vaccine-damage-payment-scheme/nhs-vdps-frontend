'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const dataRepository = require('../../common/schemas/Data')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const querystring = require('node:querystring');

module.exports = () => {

  return {
    render: async (req, res) => {
        const dataRepo = (await dataRepository()).repository
        const data = await dataRepo.fetch(req.cookies.sessionID)

        try {
            if(req.session.errors && req.session.errors.find(e => e.route === req.route.path)){
                res.redirect('visited-hospital')
                return
            }
            data.VisitedHospital = req.body.VisitedHospital
            await dataRepo.save(data)


            if(req.body.VisitedHospital === 'Yes'){
                res.redirect('hospital-details')
            } else {
                res.redirect('hospital-cya')
            }
            return
        } catch (e) {
            logRequestStart({
                level: 'error',
                message: e.message
            })
        }
        if(data.ApplyingFor === 'Deceased'){
            res.redirect('visited-hospital-died')
        } else {
            res.redirect('visited-hospital')
        }
        return

    }
  }
}
