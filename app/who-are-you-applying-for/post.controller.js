'use strict'
const { logRequestStart } = require('../../common/utils/request-logger')
const dataRepository = require('../../common/schemas/Data')
const metaRepository = require('../../common/schemas/MetaData')
const { findFile, markdown } = require('../../common/utils/markdown')
const config = require('../../common/config')
const querystring = require('node:querystring');

module.exports = () => {
  return {
    render: async (req, res) => {
      try{

        const repo = (await dataRepository()).repository
        const data = await repo.fetch(req.cookies.sessionID)
        if(req.body.ApplyingFor) {
            data.ApplyingFor = req.body.ApplyingFor
        }

        await repo.save(data)
        if(req.session.errors){
            if(req.session.errors.find(e => e.route === req.route.path) && !req.session.passage){
                res.redirect('who-are-you-applying-for')
                return
            }
        }


        if(req.body.ApplyingFor !== 'Myself'){
            req.session.passage='onBehalfOf'
        }

        if(req.body.ApplyingFor === 'Myself') {
            return res.redirect('med-records-consent')
        } else if(req.body.OnBehalfOf && req.session.passage) {
            req.session.passage = null
            data.ApplyingFor = req.body.OnBehalfOf
            await repo.save(data)

            if(req.body.OnBehalfOf === 'Deceased'){
                return res.redirect('died-med-records-consent')
            } else {
                return res.redirect('other-med-records-consent')
            }
        }

        res.redirect('who-are-you-applying-for')
        return

      } catch (e) {
        logRequestStart({
          level: 'error',
          message: e.message
        })
       // return res.redirect('back')
      }
    }
  }
}
