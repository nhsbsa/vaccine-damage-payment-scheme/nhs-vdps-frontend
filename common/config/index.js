'use strict'

exports.CORRELATION_HEADER = process.env.CORRELATION_HEADER_NAME || 'x-request-id'
exports.CONNECTOR_URL = process.env.CONNECTOR_URL
exports.S3_BUCKET = process.env.S3_BUCKET || 'test-zz'
exports.MARKDOWN_DIR = process.env.MARKDOWN_DIR || './app/page/templates'
exports.REDIS_SERVER = process.env.REDIS_SERVER || 'redis://localhost:6379'
exports.SESSION_KEY = process.env.SESSION_KEY || 'TEST KEY'
exports.REDIS_TIMEOUT = process.env.REDIS_TIMEOUT
