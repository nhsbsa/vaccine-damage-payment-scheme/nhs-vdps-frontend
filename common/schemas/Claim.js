const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {
  class Claim extends Entity {}

  const ClaimSchema = new Schema(Claim, {
    ClaimDate: { type: 'string' },
    Country: { type: 'string' },
    SelfClaimant: { type: 'string' },
    ClaimStatus: { type: 'string' },
    ConsentType: { type: 'string' },
    ReasonForClaim: { type: 'string' },
    ClaimantContactPreferences: { type: 'string[]' },
    DeclarationA: { type: 'boolean' },
    DeclarationB: { type: 'boolean' },
    NhsNumber: { type: 'string' },
    ClaimantDetails: { type: 'string' },
    VaccinatedPersonDetails: { type: 'string' },
    DisabledPersonDetails: { type: 'string' },
    Representative: { type: 'string' },
    Vaccinations: { type: 'string[]' },
    ClaimDisablement: { type: 'string' },
    GeneralPractice: { type: 'string' },
    Hospitals: { type: 'string[]' },
    HealthcareProviders: { type: 'string[]' },
    ChildSchool: { type: 'string' },
    Documents: { type: 'string[]' },
    DisplayedContactPreference: {type: 'string'},
    DeclarationFirstName: { type: 'string' },
    DeclarationLastName: { type: 'string' },
  })

  const client = await redisClient()

  const repository = client.fetchRepository(ClaimSchema)

  return {repository: repository, client: client}
  client.close()
}
