const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')


module.exports = async () => {

    class ClaimantContactPreferences extends Entity {}

    const ClaimantContactPreferencesSchema = new Schema(ClaimantContactPreferences, {
      Name: { type: 'string' }
    })

    const client = await redisClient()

    const repository = client.fetchRepository(ClaimantContactPreferencesSchema)

  return {repository: repository, client: client}
  client.close()
}
