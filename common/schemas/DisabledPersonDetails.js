const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {

class DisabledPersonDetails extends Entity {}

const DisabledPersonDetailsSchema = new Schema(DisabledPersonDetails, {
  DpTitle: { type: 'string' },
  DpSurname: { type: 'string' },
  DpMiddlename: { type: 'string' },
  DpForename: { type: 'string' },
  DpNino: { type: 'string' },
  DpAddressLine1: { type: 'string' },
  DpAddressLine2: { type: 'string' },
  DpTownOrCity: { type: 'string' },
  DpCounty: { type: 'string' },
  DpPostcode: { type: 'string' },
  DpMobileTelNo: { type: 'string' },
  DpHomeTelNo: { type: 'string' },
  DpWorkTelNo: { type: 'string' },
  DpEmailAddress: { type: 'string' },
  DpDateOfBirth: { type: 'string' },
  DpDateOfDeath: { type: 'string' },
  DpDeathCertificate: { type: 'boolean' },
  DisplayedAddress: {type: 'string' }
})

    const client = await redisClient()
    const repository = client.fetchRepository(DisabledPersonDetailsSchema)

  return {repository: repository, client: client}
  client.close()
}
