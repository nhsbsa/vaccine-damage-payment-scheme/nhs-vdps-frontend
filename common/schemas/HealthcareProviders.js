const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {

class HealthcareProviders extends Entity {}

    const HealthcareProvidersSchema = new Schema(HealthcareProviders, {
      ClinicName: { type: 'string' },
      ClinicAddressLine1: { type: 'string' },
      ClinicAddressLine2: { type: 'string' },
      ClinicTownOrCity: { type: 'string' },
      ClinicCounty: { type: 'string' },
      ClinicPostcode: { type: 'string' },
      ClinicTelNo: { type: 'string' },
      ClinicConsultant: { type: 'string' },
      ClinicConsultantTelNo: { type: 'string' },
      ClinicConsultantEmail: { type: 'string' },
      ClinicCount: { type: 'number'},
      ClinicDisplayedAddress: { type: 'string' },
      DisplayOnline: { type:'boolean' }
    })
    const client = await redisClient()

    const repository = client.fetchRepository(HealthcareProvidersSchema)

  return {repository: repository, client: client}
  client.close()
}
