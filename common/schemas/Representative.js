const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')


module.exports = async () => {

  class Representative extends Entity {}

  const RepresentativeSchema = new Schema(Representative, {
    RepresentativeTitle: { type: 'string' },
    RepresentativeSurname: { type: 'string' },
    RepresentativeMiddlename: { type: 'string' },
    RepresentativeForename: { type: 'string' },
    RepresentativeNino: { type: 'string' },
    RepresentativeDateOfBirth: { type: 'date' },
    RepresentativeRelationship: { type: 'string' },
    RepresentativeAddressLine1: { type: 'string' },
    RepresentativeAddressLine2: { type: 'string' },
    RepresentativeTownOrCity: { type: 'string' },
    RepresentativeCounty: { type: 'string' },
    RepresentativePostcode: { type: 'string' },
    RepresentativeMobileTelNo: { type: 'string' },
    RepresentativeHomeTelNo: { type: 'string' },
    RepresentativeWorkTelNo: { type: 'string' },
    RepresentativeEmailAddress: { type: 'string' },
    RepresentativePlaceOfStay: {type: 'string'},
    RepresentativeQuestion: {type: 'string'},
    DisplayedContactInformation: { type: 'string' },
    DisplayedAddress: { type: 'string'}
  })
  const client = await redisClient()

  const repository = client.fetchRepository(RepresentativeSchema)
  return {repository: repository, client: client}
  client.close()

}
