'use strict'
const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {

  class VaccinatedPersonDetails extends Entity {}

  const VaccinatedPersonDetailsSchema = new Schema(VaccinatedPersonDetails, {
    VpTitle: { type: 'string' },
    VpSurname: { type: 'string' },
    VpMiddlename: { type: 'string' },
    VpForename: { type: 'string' },
    VpNino: { type: 'string' },
    VpAddressLine1: { type: 'string' },
    VpAddressLine2: { type: 'string' },
    VpTownOrCity: { type: 'string' },
    VpCounty: { type: 'string' },
    VpPostcode: { type: 'string' },
    VpMobileTelNo: { type: 'string' },
    VpHomeTelNo: { type: 'string' },
    VpWorkTelNo: { type: 'string' },
    VpEmailAddress: { type: 'string' },
    VpDateOfBirth: { type: 'string' }
  })
    const client = await redisClient()
    const repository = client.fetchRepository(VaccinatedPersonDetailsSchema)

  return {repository: repository, client: client}
  client.close()
}
