const { Entity, Schema } = require('redis-om')
const redisClient = require('./redis-client')

module.exports = async () => {
    class VaccineNotes extends Entity {}

    const VaccineNotesSchema = new Schema(VaccineNotes, {
        Description: { type: 'string' },
        Diagnosis: { type: 'string[]' }
    })
    const client = await redisClient()

    const repository = client.fetchRepository(VaccineNotesSchema)

  return {repository: repository, client: client}
  client.close()

}