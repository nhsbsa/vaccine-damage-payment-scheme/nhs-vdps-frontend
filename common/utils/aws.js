var AWS = require('aws-sdk')
const { logRequestStart } = require('../utils/request-logger')
const config = require('../config/index')

if (process.argv[2] == 'is_offline'){
    AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: 'nhsdev' })
    AWS.config.update({ region: 'eu-west-2' })
}

module.exports = {
  secretManager: async (key) => {
    try {
      const secrets = new AWS.SecretsManager()
      const secretValue = await secrets.getSecretValue({ SecretId: key }).promise()
      return secretValue.SecretString
    } catch (e) {
      logRequestStart({
        level: 'info',
        message: `secretManager - Error(s): Failed initializing AWS Secret Manager ${e.code}`
      })
      return null
    }
  }

}
