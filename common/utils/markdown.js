const MarkdownIt = require('markdown-it')
const config = require('../config/index')
const fs = require('fs')
/**
 * Returns rendered markdown from a buffer
 *
 * @param {String} path - Buffer of the file contents to render
 */

module.exports = {
  markdown: (contents) => {
    const md = new MarkdownIt({html:true})
    return md.render(contents)
  },
  findFile: (name) => {
//    const file = fs.readdirSync(`${config.MARKDOWN_DIR}).find(fn => fn.startsWith(name))
    return fs.readFileSync(name !== 'index' ? `./app/${name}/templates/index.njk` : `./app/index/templates/index.njk`)
  }
}
