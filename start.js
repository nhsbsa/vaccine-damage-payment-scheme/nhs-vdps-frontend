'use strict'

const path = require('path')
const fs = require('fs')
const logger = require('pino')()
const throng = require('throng')
const server = require('./server')

const pidFile = path.join(__dirname, '/.start.pid')
const fileOptions = { encoding: 'utf-8' }
let pid
const util = require('util');
const exec = util.promisify(require('child_process').exec);


/**
 * Throng is a wrapper around node cluster
 * https://github.com/hunterloftis/throng
 */
function start () {
  server.start()
/*  throng({
    workers: process.env.NODE_WORKER_COUNT || 1,
    master: startMaster,
    start: startWorker
  })*/
}

/**
 * Start master process
 */
function startMaster () {
  logger.info(`Master started. PID: ${process.pid}`)
  process.on('SIGINT', () => {
    logger.info(`Master exiting`)
    process.exit()
  })
}

/**
 * Start cluster worker. Log start and exit
 * @param  {Number} workerId
 */
function startWorker (workerId) {
  server.start()

  logger.info(`Started worker ${workerId}, PID: ${process.pid}`)

  process.on('SIGINT', () => {
    logger.info(`Worker ${workerId} exiting...`)
    process.exit()
  })
}

/**
 * Make sure all child processes are cleaned up
 */
// eslint-disable-next-line no-unused-vars
function onInterrupt () {
  pid = fs.readFileSync(pidFile, fileOptions)
  fs.unlink(pidFile, (err) => {
    if (err) throw err
    console.log('File successfully deleted')
  })
  process.kill(pid, 'SIGTERM')
  process.exit()
}

/**
 * Keep track of processes, and clean up on SIGINT
 */
function monitor () {
   fs.writeFileSync(pidFile, process.pid.toString(), fileOptions)
   process.on('SIGINT', onInterrupt)
}

monitor()

start()
