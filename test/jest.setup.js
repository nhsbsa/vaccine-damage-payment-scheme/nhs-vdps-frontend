// Test environment configurations
process.env.LOG_LEVEL = 'error';
process.env.NODE_ENV = 'test';



// Inject mocks for all the tests
jest.mock('express');